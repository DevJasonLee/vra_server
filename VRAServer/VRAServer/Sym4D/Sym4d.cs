﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace VRAServer.Sym4D
{
    class Sym4D
    {
        ////////////////////////////////////////////////////////////////////////////////////
        /*         Sym4D-X100 Device를 사용하기 위한 Dll File 선언부                      */
        ////////////////////////////////////////////////////////////////////////////////////
        const string Sym4D_Dll = "Sym4D.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0

        // 함수의 리턴값 정의 : TRUE = 성공, FALSE = 실패
        ////////////////////////////////////////////////////////////////////////////////////
    }
}
