﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRAServer.Common
{
    class S4DLoader
    {
        private float m_FPS;
        private long m_totalFrame;

        private sbyte[] m_rolls;       // -100 ~ 100
        private sbyte[] m_pitches;     // -100 ~ 100
        private sbyte[] m_winds;       // 0    ~ 100

        public S4DLoader(String fileName)
        {

            using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                m_totalFrame = reader.BaseStream.Length / 16 - 3;
                m_rolls = new sbyte[m_totalFrame];
                m_pitches = new sbyte[m_totalFrame];
                m_winds = new sbyte[m_totalFrame];

                for (int count = 0; count < m_totalFrame; ++count)
                {
                    int[] bytes = new int[8];
                    for (int i = 0; i < 8; ++i)
                    {
                        bytes[i] = reader.ReadByte() - 100;
                    }

                    m_rolls[count] = (sbyte)bytes[0];
                    m_pitches[count] = (sbyte)bytes[1];
                    m_winds[count] = (sbyte)bytes[2];

                    UInt64 mediaFrame = reader.ReadUInt64();
                }

                for (int i = 0; i < 8; ++i) reader.ReadByte();
                m_FPS = (float)reader.ReadUInt64();
                if (m_FPS == 29f) m_FPS = 29.97f;
            }

        }

        public long GetTotalFrame()
        {
            return m_totalFrame;
        }

        public float GetFPS()
        {
            return m_FPS;
        }

        public int getRoll(long frame)
        {
            return m_rolls[frame];
        }

        public int getPitches(long frame)
        {
            return m_pitches[frame];
        }

        public int getWinds(long frame)
        {
            return m_winds[frame];
        }

        // 플레이시간(단위 : 초)
        public float getPlayTime()
        {
            return (float)GetTotalFrame() / (float)GetFPS();
        }
    }
}
