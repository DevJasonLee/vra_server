﻿using System;
using System.IO;
using System.Threading;
using VRAServer.Common;
using System.Runtime.InteropServices;

namespace VRAServer.Sym4D
{
    public class MoviePlay
    {
        private int HMDNo, ContentNo, m_control, StartTime;
        private Config config;
        private volatile bool isStop = false;
        private System.Object lockThis = new System.Object();
        private S4DLoader loader;

        public MoviePlay(Config config, int HMDNo, int ContentNo, int StartTime)
        {
            this.config = config;
            this.HMDNo = HMDNo;
            this.ContentNo = ContentNo;
            this.StartTime = StartTime;
            this.m_control = 0;

            String fileName = String.Format("Movies/{0}.s4d", ContentNo);
            if (File.Exists(fileName)) loader = new S4DLoader(fileName);
        }

        public float GetPlayTime()
        {
            return loader.getPlayTime();
        }

        public void Control(int control)
        {
            Interlocked.Exchange(ref m_control, control);
        }

        public void Play()
        {
            bool bW;
            bool bX;

            int motionPort, windPort;

            Console.WriteLine("SDK를 불러오는 중입니다.");
            switch (HMDNo) {
                case 1:
                    if (!config.motion1P.Equals("사용안함"))
                        motionPort = Int32.Parse(config.motion1P.Replace("COM", ""));
                    else motionPort = -1;

                    if (!config.wind1P.Equals("사용안함"))
                        windPort = Int32.Parse(config.wind1P.Replace("COM", ""));
                    else windPort = -1;

                    bW = Sym4D_W1.Sym4D_W_StartContents(windPort); Thread.Sleep(100);
                    bX = Sym4D_X1.Sym4D_X_StartContents(motionPort); Thread.Sleep(100);
                    if (bW == false || bX == false)
                    {
                        //return;
                    }
                    Sym4D_W1.Sym4D_W_Init(); Thread.Sleep(100);
                    Sym4D_X1.Sym4D_X_Init(); Thread.Sleep(100);
                    Sym4D_W1.Sym4D_W_SetConfig(100); Thread.Sleep(100);
                    Sym4D_X1.Sym4D_X_SetConfig(100, 100); Thread.Sleep(100);
                    break;
                case 2:
                    if (!config.motion2P.Equals("사용안함"))
                        motionPort = Int32.Parse(config.motion2P.Replace("COM", ""));
                    else motionPort = -1;

                    if (!config.wind2P.Equals("사용안함"))
                        windPort = Int32.Parse(config.wind2P.Replace("COM", ""));
                    else windPort = -1;
                    bW = Sym4D_W2.Sym4D_W_StartContents(windPort); Thread.Sleep(100);
                    bX = Sym4D_X2.Sym4D_X_StartContents(motionPort); Thread.Sleep(100);
                    if ( bW == false || bX == false )
                    {
                        //return;
                    }
                    Sym4D_W2.Sym4D_W_Init(); Thread.Sleep(100);
                    Sym4D_X2.Sym4D_X_Init(); Thread.Sleep(100);
                    Sym4D_W2.Sym4D_W_SetConfig(100); Thread.Sleep(100);
                    Sym4D_X2.Sym4D_X_SetConfig(100, 100); Thread.Sleep(100);
                    break;
                case 3:
                    if (!config.motion3P.Equals("사용안함"))
                        motionPort = Int32.Parse(config.motion3P.Replace("COM", ""));
                    else motionPort = -1;

                    if (!config.wind3P.Equals("사용안함"))
                        windPort = Int32.Parse(config.wind3P.Replace("COM", ""));
                    else windPort = -1;
                    bW = Sym4D_W3.Sym4D_W_StartContents(windPort); Thread.Sleep(100);
                    bX = Sym4D_X3.Sym4D_X_StartContents(motionPort); Thread.Sleep(100);
                    if ( bW == false || bX == false )
                    {
                        //return;
                    }
                    Sym4D_W3.Sym4D_W_Init(); Thread.Sleep(100);
                    Sym4D_X3.Sym4D_X_Init(); Thread.Sleep(100);
                    Sym4D_W3.Sym4D_W_SetConfig(100); Thread.Sleep(100);
                    Sym4D_X3.Sym4D_X_SetConfig(100, 100); Thread.Sleep(100);
                    break;
            }


                    Console.Write("FPS:" + loader.GetFPS() + " ");  // 29? 29.97?
                Console.Write("TotalFrame:" + loader.GetTotalFrame() + " ");
                Console.Write("PlayTime(sec):" + loader.getPlayTime() + "");
                Console.WriteLine();

                // 모션 플레이
                //int startTick = Environment.TickCount & Int32.MaxValue;
                int startTick = StartTime;

                long prevFrame = 0;

                while (!isStop)
                {

                    int nowTick = Environment.TickCount & Int32.MaxValue;
                    int playTick = nowTick - startTick;

                    long nowFrame = (int)((float)playTick * (float)loader.GetFPS() * 0.001f);

                    if (nowFrame >= loader.GetTotalFrame())
                    {
                        isStop = true;
                        break;
                    }

                    if (prevFrame == nowFrame) continue;
                    prevFrame = nowFrame;

                    // 일시정지
                    if (m_control == 1) continue;

                    // 종료
                    if (m_control == 3)
                    {
                        isStop = true;
                        break;
                    }

                    if (nowFrame >= 0 && nowFrame < loader.GetTotalFrame())
                    {                         // Sym4D 실행
                        lock (lockThis)
                        {
                            Console.Write(" HMD:" + HMDNo);
                            Console.Write(" Frame:" + (int)nowFrame);
                            Console.Write(" Roll:" + loader.getRoll(nowFrame));
                            Console.Write(" Pitch:" + loader.getPitches(nowFrame));
                            Console.Write(" Wind:" + loader.getWinds(nowFrame));
                            Console.Write(" Time(sec):" + (playTick * 0.001));
                            Console.WriteLine();

                            switch (HMDNo)
                            {
                                case 1:
                                    Sym4D_W1.Sym4D_W_SendMosionData(loader.getWinds(nowFrame));
                                    Sym4D_X1.Sym4D_X_SendMosionData(loader.getRoll(nowFrame), loader.getPitches(nowFrame));
                                    break;
                                case 2:
                                    Sym4D_W2.Sym4D_W_SendMosionData(loader.getWinds(nowFrame));
                                    Sym4D_X2.Sym4D_X_SendMosionData(loader.getRoll(nowFrame), loader.getPitches(nowFrame));
                                    break;
                                case 3:
                                    Sym4D_W3.Sym4D_W_SendMosionData(loader.getWinds(nowFrame));
                                    Sym4D_X3.Sym4D_X_SendMosionData(loader.getRoll(nowFrame), loader.getPitches(nowFrame));
                                    break;
                            }
                        }
                    }
                } // While 종료

                Thread.Sleep(100);

                switch (HMDNo)
                {
                    case 1:
                        Sym4D_W1.Sym4D_W_SendMosionData(0);
                        Sym4D_X1.Sym4D_X_SendMosionData(0, 0);
                        Thread.Sleep(1000);
                        Sym4D_W1.Sym4D_W_EndContents();
                        Sym4D_W1.Sym4D_X_EndContents();
                        break;
                    case 2:
                        Sym4D_W2.Sym4D_W_SendMosionData(0);
                        Sym4D_X2.Sym4D_X_SendMosionData(0, 0);
                        Thread.Sleep(1000);
                        Sym4D_W2.Sym4D_W_EndContents();
                        Sym4D_W2.Sym4D_X_EndContents();
                        break;
                    case 3:
                        Sym4D_W3.Sym4D_W_SendMosionData(0);
                        Sym4D_X3.Sym4D_X_SendMosionData(0, 0);
                        Thread.Sleep(1000);
                        Sym4D_W3.Sym4D_W_EndContents();
                        Sym4D_W3.Sym4D_X_EndContents();
                        break;
                }

            

        }

        // Thread 중지 설정
        public void ThreadStop()
        {
            this.isStop = true;
        }

        public bool IsStopped()
        {
            return this.isStop;
        }

    }

    class Sym4D_W1
    {
        const string Sym4D_Dll = "Sym4D_W1.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }

    class Sym4D_W2
    {
        const string Sym4D_Dll = "Sym4D_W2.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }

    class Sym4D_W3
    {
        const string Sym4D_Dll = "Sym4D_W3.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }

    class Sym4D_X1
    {
        const string Sym4D_Dll = "Sym4D_X1.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }

    class Sym4D_X2
    {
        const string Sym4D_Dll = "Sym4D_X2.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }

    class Sym4D_X3
    {
        const string Sym4D_Dll = "Sym4D_X3.dll";

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_X_Find();                                    // Sym4D-X100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_W_Find();                                    // Sym4D-W100 장비가 연결되어 있는 Serial Port를 찾는 함수		리턴값의 정의  예) 25 -> 장비가 연결된 Serial Port = COM25 

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_Init();                                   // Sym4D-X100 장치의 포지션을 중심으로 이동

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_Init();                                   // Sym4D-W100 장치의 풍량을 0로 초기화

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SetConfig(int mRoll, int mPitch);         // Sym4D-X100 장치의 최대 허용 각도를 설정  : mRoll, mPitch의 범위 = 0 ~ 100 (0도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SetConfig(int mWind);                     // Sym4D-W100 장치의 최대 풍량 설정  : mWind 범위 = 0 ~ 100 (0% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_SendMosionData(int mRoll, int mPitch);    // Sym4D-X100 장치에 모션 Data를 전달  : mRoll, mPitch의 범위 = -100 ~ 100 (-10도 ~ 10도)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_SendMosionData(int mWind);                // Sym4D-W100 장치에 바람 Data를 전달  : mWind의 범위 = -100 ~ 100 (-100% ~ 100%)

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_StartContents(int sComPortName);          // Sym4D-X100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_StartContents(int sComPortName);          // Sym4D-W100 COM Port Open  및 컨텐츠 시작을 장치에 전달		sComPortName 인자의 예) COM5 -> 5, COM17 -> 17

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_X_EndContents();                            // Sym4D-X100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern bool Sym4D_W_EndContents();                            // Sym4D-W100 COM Port Close 및 컨텐츠 종료를 장치에 전달

        [DllImport(Sym4D_Dll, CallingConvention = CallingConvention.Cdecl)]
        public static extern int Sym4D_API_Version();                             // Dll API 모듈의 버전 정보			리턴값의 정의  예) 1000 = Ver 1.00.0      10000 = Ver 10.00.0
    }


}
