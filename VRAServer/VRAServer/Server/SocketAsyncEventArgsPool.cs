﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

namespace VRAServer.Common
{
    class SocketAsyncEventArgsPool
    {
        private Stack<SocketAsyncEventArgs> m_pool;

        public SocketAsyncEventArgsPool(int capacity)
        {
            Console.WriteLine("SocketAsyncEventArgsPool-SocketAsyncEventArgsPool");
            m_pool = new Stack<SocketAsyncEventArgs>(capacity);
        }

        public void Push(SocketAsyncEventArgs item)
        {
            Console.WriteLine("SocketAsyncEventArgsPool-Push");
            if (item == null) { throw new ArgumentNullException("SocketAsyncEventArgsPool에 null값이 추가됨."); }
            lock (m_pool)
            {
                m_pool.Push(item);
            }
        }

        public SocketAsyncEventArgs Pop()
        {
            Console.WriteLine("SocketAsyncEventArgsPool-Pop");
            lock (m_pool)
            {
                return m_pool.Pop();
            }
        }

        public int Count
        {
            get { return m_pool.Count; }
        }

    }
}