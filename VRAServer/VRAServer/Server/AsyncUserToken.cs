﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;

namespace VRAServer.Common
{
    class AsyncUserToken
    {
        public Socket Socket { get; internal set; }
        private List<byte> buffer;
        private int strIndex;

        private int HMDNo;

        public int GetHMDNo()
        {
            return HMDNo;
        }

        public void SetHMDNo(int HMDNo)
        {
            this.HMDNo = HMDNo;
        }


        public AsyncUserToken()
        {
            buffer = new List<byte>();
        }

        public void AppendData(byte[] bytes)
        {
            buffer.AddRange(bytes);
        }              

        public List<byte[]> GetPackets()
        {
            List<byte[]> packets = new List<byte[]>();

            while (buffer.Count >= 12)
            {
                ReadBegin();
                Int32 stx = ReadInt32();
                Int32 len = ReadInt32();
                Int32 cmd = ReadInt32();

                // STX가 맞지 않을 경우
                if ( stx != 0x12344321 )
                {
                    FindSTX();
                    continue;
                }

                // LEN값이 잘못될 경우
                if (len < 12)
                {
                    FindSTX();
                    continue;
                }

                if (buffer.Count >= len)
                {
                    byte[] packet = buffer.GetRange(0, len).ToArray();
                    packets.Add(packet);
                    buffer.RemoveRange(0, len); 
                } else {
                    break;
                }

            }

            return packets;
        }

        private void FindSTX()
        {            
            // 현재 패킷을 버리고 STX를 찾는다.
            while (buffer.Count > 4) {
                buffer.RemoveAt(0);
                byte[] bytes = buffer.GetRange(0, sizeof(Int32)).ToArray();
                int stx = BitConverter.ToInt32(bytes, 0);
                if (stx == 0x12344321) break;
            }

        }

        private void ReadBegin()
        {
            strIndex = 0;
        }

        private Int32 ReadInt32()
        {
            Int32 value = BitConverter.ToInt32(buffer.GetRange(strIndex, sizeof(Int32)).ToArray(), 0);
            strIndex += sizeof(Int32);
            return value;
        }

    }


}