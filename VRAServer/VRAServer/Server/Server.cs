﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace VRAServer.Common
{
    delegate byte[] EventDelegateProc(byte[] packet);

    class Server
    {
        private int m_numConnections;
        private int m_receiveBufferSize;
        private BufferManager m_bufferManager;
        private const int opsToPreAlloc = 2;
        private Socket listenSocket;
        private SocketAsyncEventArgsPool m_readWritePool;
        private int m_totalBytesRead;
        private int m_numConnectedSockets;
        private Semaphore m_maxNumberAcceptedClients;        
        public event EventDelegateProc eventDelegateProc;
        private List<SocketAsyncEventArgs> sockets;
        private Boolean bIsListenSocketDisposed;

        public Server(int numConnections, int receiveBufferSize)
        {
            m_totalBytesRead = 0;
            m_numConnectedSockets = 0;
            m_numConnections = numConnections;
            m_receiveBufferSize = receiveBufferSize;
            m_bufferManager = new BufferManager(receiveBufferSize * numConnections * opsToPreAlloc, receiveBufferSize);
            m_readWritePool = new SocketAsyncEventArgsPool(numConnections);
            m_maxNumberAcceptedClients = new Semaphore(numConnections, numConnections);
            bIsListenSocketDisposed = false;
        }

        public void Init()
        {
            Console.WriteLine("Init");
            m_bufferManager.InitBuffer();
            sockets = new List<SocketAsyncEventArgs>();

            SocketAsyncEventArgs readWriteEventArg;

            for (int i = 0; i < m_numConnections; i++)
            {
                readWriteEventArg = new SocketAsyncEventArgs();
                readWriteEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(IO_Completed);
                readWriteEventArg.UserToken = new AsyncUserToken();
                m_bufferManager.SetBuffer(readWriteEventArg);
                m_readWritePool.Push(readWriteEventArg);
            }

        }

        public void Start(IPEndPoint localEndPoint)
        {
            Console.WriteLine("Start");
            bIsListenSocketDisposed = false;
            listenSocket = new Socket(localEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listenSocket.Bind(localEndPoint);
            listenSocket.Listen(100);
            StartAccept(null);            
        }


        public void StartAccept(SocketAsyncEventArgs acceptEventArg)
        {
            if (bIsListenSocketDisposed) return;


            Console.WriteLine("StartAccept");
            if (acceptEventArg == null)
            {
                acceptEventArg = new SocketAsyncEventArgs();
                acceptEventArg.Completed += new EventHandler<SocketAsyncEventArgs>(AcceptEventArg_Completed);
            }
            else
            {
                // 소켓 재사용을 위해 비워두기
                acceptEventArg.AcceptSocket = null;
            }

            m_maxNumberAcceptedClients.WaitOne();            
            bool willRaiseEvent = listenSocket.AcceptAsync(acceptEventArg);
            if (!willRaiseEvent)
            {
                ProcessAccept(acceptEventArg);
            }
        }

        void AcceptEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            Console.WriteLine("AcceptEventArg_Completed");
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            Console.WriteLine("ProcessAccept");
            Interlocked.Increment(ref m_numConnectedSockets);
            Console.WriteLine("클라이언트 연결 접속({0} 클라이언트 접속중)", m_numConnectedSockets);

            SocketAsyncEventArgs readEventArgs = m_readWritePool.Pop();
            sockets.Add(readEventArgs);
            ((AsyncUserToken)readEventArgs.UserToken).Socket = e.AcceptSocket;

            bool willRaiseEvent = e.AcceptSocket.ReceiveAsync(readEventArgs);
            if (!willRaiseEvent)
            {
                ProcessReceive(readEventArgs);
            }

            StartAccept(e);
        }

        void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            Console.WriteLine("IO_Completed");
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Receive:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.Send:
                    ProcessSend(e);
                    break;
                default:
                    throw new ArgumentException("클라이언트의 작업이 수신 또는 전송이 아닙니다.");
            }

        }


        // 비동기 통신 받기
        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            Console.WriteLine("ProcessReceive");
            AsyncUserToken token = (AsyncUserToken)e.UserToken;
            if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
                Interlocked.Add(ref m_totalBytesRead, e.BytesTransferred);
                Console.WriteLine("수신 데이터 : 전체 {0} bytes, {1} bytes", m_totalBytesRead, e.BytesTransferred);

                // 수신 데이터 처리
                byte[] recvBytes = new byte[e.BytesTransferred];
                Buffer.BlockCopy(e.Buffer, e.Offset, recvBytes, 0, e.BytesTransferred);
                token.AppendData(recvBytes);
                
                List<byte[]>  packets = token.GetPackets();

                if (packets != null)
                {
                    MemoryStream memory = new MemoryStream();
                    BinaryWriter writer = new BinaryWriter(memory);

                    foreach (byte[] packet in packets)
                    {
                        // 추후 메세드로 분리
                        {
                            BinaryReader reader = new BinaryReader(new MemoryStream(packet));
                            Int32 stx = reader.ReadInt32();
                            Int32 len = reader.ReadInt32();
                            Int32 cmd = reader.ReadInt32();
                            switch (cmd)
                            {
                                case 0x00000010:
                                    // 동일한 HMDNo가 있을 경우...
                                    Int32 HMDNo = reader.ReadInt32();
                                    token.SetHMDNo(HMDNo);
                                    break;
                                default:
                                    break;
                            }
                        }

                        byte[] rst = eventDelegateProc(packet);
                        if (rst != null) writer.Write(rst);
                    }

                    //if (memory.Length > 0)
                    //{
                        byte[] sendBytes = memory.ToArray();
                        Buffer.BlockCopy(sendBytes, 0, e.Buffer, e.Offset, sendBytes.Length);
                        e.SetBuffer(e.Offset, sendBytes.Length);

                        bool willRaiseEvent = token.Socket.SendAsync(e);
                        if (!willRaiseEvent)
                        {
                            ProcessSend(e);
                        }
                    //}
                }

            }
            else
            {
                CloseClientSocket(e);
            }
        }

        // 비동기 통신 보내기
        private void ProcessSend(SocketAsyncEventArgs e)
        {
            Console.WriteLine("ProcessSend");
            if (e.SocketError == SocketError.Success)
            {
                AsyncUserToken token = (AsyncUserToken)e.UserToken;

                e.SetBuffer(e.Offset, m_receiveBufferSize);

                // 클라이언트로부터 다음 데이터 블록을 읽기
                bool willRaiseEvent = token.Socket.ReceiveAsync(e);
                if (!willRaiseEvent)
                {
                    ProcessReceive(e);
                }
            }
            else
            {
                CloseClientSocket(e);
            }

        }

        // 모든 HMD에 전송
        public void SendAll(MemoryStream memory)
        {
            Console.WriteLine("Send");
            byte[] sendBytes = memory.ToArray();
            foreach (SocketAsyncEventArgs e in sockets)
            {
                AsyncUserToken token = (AsyncUserToken)e.UserToken;
                token.Socket.BeginSend(sendBytes, 0, sendBytes.Length, SocketFlags.None, new AsyncCallback(SendCallback), e);
            }
        }

        // 특정 HMD에 전송
        public void SendOne(MemoryStream memory, int HMDNo)
        {
            Console.WriteLine("Send HMDNo:" + HMDNo);
            byte[] sendBytes = memory.ToArray();
            foreach (SocketAsyncEventArgs e in sockets)
            {                
                AsyncUserToken token = (AsyncUserToken)e.UserToken;
                if (token.GetHMDNo() == HMDNo || token.GetHMDNo() == -1)
                {
                    token.Socket.BeginSend(sendBytes, 0, sendBytes.Length, SocketFlags.None, new AsyncCallback(SendCallback), e);
                }
            }
        }

        private void SendCallback(IAsyncResult iar)
        {
            Console.WriteLine("SendCallback");
            try
            {
                SocketAsyncEventArgs e = (SocketAsyncEventArgs)iar.AsyncState;
                AsyncUserToken token = (AsyncUserToken)e.UserToken;
                Socket socket = ((AsyncUserToken)e.UserToken).Socket;

                int BytesTransferred = socket.EndSend(iar);
                Console.WriteLine("Sent {0} bytes to client.", BytesTransferred);

                // TODO: 소켓 오류처리

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }


        private void CloseClientSocket(SocketAsyncEventArgs e)
        {
            Console.WriteLine("CloseClientSocket");
            AsyncUserToken token = e.UserToken as AsyncUserToken;

            // 클라이언트와 연결된 소켓 닫기
            try
            {
                token.Socket.Shutdown(SocketShutdown.Send);
            }
            // 클라이언트 프로세스가 이미 닫힌 경우 예외 발생
            catch (Exception) { }
            token.Socket.Close();

            Interlocked.Decrement(ref m_numConnectedSockets);
            m_maxNumberAcceptedClients.Release();
            Console.WriteLine("클라이언트 연결 종료({0} 클라이언트 접속중)", m_numConnectedSockets);            
            
            m_readWritePool.Push(e);
            sockets.Remove(e);
        }

        public void CloseAllClientSocket()
        {

            //foreach (SocketAsyncEventArgs e in sockets)
            //{
            //    CloseClientSocket(e);
            //}
            if( listenSocket.Connected ) listenSocket.Shutdown(SocketShutdown.Both);
            listenSocket.Close();
            bIsListenSocketDisposed = true;

        }



    }
}
