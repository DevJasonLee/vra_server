﻿using VRAServer.Common;

namespace VRAServer
{
    partial class VRAForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VRAForm));
            this.pnBack = new System.Windows.Forms.Panel();
            this.pnBeginFree = new System.Windows.Forms.Panel();
            this.pbFreePlayer2 = new VRAServer.Common.TouchButton();
            this.pbFreePlayer1 = new VRAServer.Common.TouchButton();
            this.btFreeStart = new System.Windows.Forms.Button();
            this.pnBeginCost = new System.Windows.Forms.Panel();
            this.pbCoin1 = new System.Windows.Forms.PictureBox();
            this.pbCoin2 = new System.Windows.Forms.PictureBox();
            this.pbCoin3 = new System.Windows.Forms.PictureBox();
            this.pbCoin4 = new System.Windows.Forms.PictureBox();
            this.pbCoin5 = new System.Windows.Forms.PictureBox();
            this.pbPlayer2 = new VRAServer.Common.TouchButton();
            this.btCostStart = new System.Windows.Forms.Button();
            this.pbPlayer1 = new VRAServer.Common.TouchButton();
            this.pnMovieSelect = new System.Windows.Forms.Panel();
            this.pbMovie3 = new VRAServer.Common.TouchButton();
            this.pbMovie2 = new VRAServer.Common.TouchButton();
            this.pbMovie1 = new VRAServer.Common.TouchButton();
            this.btSelectMovie = new System.Windows.Forms.Button();
            this.pnMovieGuide = new System.Windows.Forms.Panel();
            this.btPreInfo = new System.Windows.Forms.Button();
            this.pnMoviePlay = new System.Windows.Forms.Panel();
            this.pbForceFinish = new VRAServer.Common.TouchButton();
            this.btMoviePlay = new System.Windows.Forms.Button();
            this.pnFinish = new System.Windows.Forms.Panel();
            this.btFinish = new System.Windows.Forms.Button();
            this.pnBeginFree.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFreePlayer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFreePlayer1)).BeginInit();
            this.pnBeginCost.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayer1)).BeginInit();
            this.pnMovieSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie1)).BeginInit();
            this.pnMovieGuide.SuspendLayout();
            this.pnMoviePlay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbForceFinish)).BeginInit();
            this.pnFinish.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnBack
            // 
            this.pnBack.BackColor = System.Drawing.Color.Transparent;
            this.pnBack.Location = new System.Drawing.Point(1000, 0);
            this.pnBack.Name = "pnBack";
            this.pnBack.Size = new System.Drawing.Size(24, 24);
            this.pnBack.TabIndex = 0;
            this.pnBack.Click += new System.EventHandler(this.Magic_Click);
            // 
            // pnBeginFree
            // 
            this.pnBeginFree.BackColor = System.Drawing.Color.Transparent;
            this.pnBeginFree.Controls.Add(this.pbFreePlayer2);
            this.pnBeginFree.Controls.Add(this.pbFreePlayer1);
            this.pnBeginFree.Controls.Add(this.btFreeStart);
            this.pnBeginFree.Location = new System.Drawing.Point(0, 300);
            this.pnBeginFree.Name = "pnBeginFree";
            this.pnBeginFree.Size = new System.Drawing.Size(1024, 400);
            this.pnBeginFree.TabIndex = 1;
            this.pnBeginFree.Visible = false;
            // 
            // pbFreePlayer2
            // 
            this.pbFreePlayer2.Image = global::VRAServer.Properties.Resources.freeplayer2;
            this.pbFreePlayer2.Location = new System.Drawing.Point(524, 0);
            this.pbFreePlayer2.Name = "pbFreePlayer2";
            this.pbFreePlayer2.Size = new System.Drawing.Size(476, 378);
            this.pbFreePlayer2.TabIndex = 2;
            this.pbFreePlayer2.TabStop = false;
            this.pbFreePlayer2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbFreePlayer2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // pbFreePlayer1
            // 
            this.pbFreePlayer1.Image = global::VRAServer.Properties.Resources.freeplayer1;
            this.pbFreePlayer1.Location = new System.Drawing.Point(48, 0);
            this.pbFreePlayer1.Name = "pbFreePlayer1";
            this.pbFreePlayer1.Size = new System.Drawing.Size(476, 378);
            this.pbFreePlayer1.TabIndex = 1;
            this.pbFreePlayer1.TabStop = false;
            this.pbFreePlayer1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbFreePlayer1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // btFreeStart
            // 
            this.btFreeStart.Location = new System.Drawing.Point(3, 3);
            this.btFreeStart.Name = "btFreeStart";
            this.btFreeStart.Size = new System.Drawing.Size(39, 57);
            this.btFreeStart.TabIndex = 0;
            this.btFreeStart.Text = "무료시작";
            this.btFreeStart.UseVisualStyleBackColor = true;
            this.btFreeStart.Click += new System.EventHandler(this.btFreeStart_Click);
            // 
            // pnBeginCost
            // 
            this.pnBeginCost.BackColor = System.Drawing.Color.Transparent;
            this.pnBeginCost.Controls.Add(this.pbCoin1);
            this.pnBeginCost.Controls.Add(this.pbCoin2);
            this.pnBeginCost.Controls.Add(this.pbCoin3);
            this.pnBeginCost.Controls.Add(this.pbCoin4);
            this.pnBeginCost.Controls.Add(this.pbCoin5);
            this.pnBeginCost.Controls.Add(this.pbPlayer2);
            this.pnBeginCost.Controls.Add(this.btCostStart);
            this.pnBeginCost.Controls.Add(this.pbPlayer1);
            this.pnBeginCost.Location = new System.Drawing.Point(0, 300);
            this.pnBeginCost.Name = "pnBeginCost";
            this.pnBeginCost.Size = new System.Drawing.Size(1024, 300);
            this.pnBeginCost.TabIndex = 2;
            this.pnBeginCost.Visible = false;
            // 
            // pbCoin1
            // 
            this.pbCoin1.BackColor = System.Drawing.Color.Transparent;
            this.pbCoin1.Image = global::VRAServer.Properties.Resources._0;
            this.pbCoin1.Location = new System.Drawing.Point(656, 0);
            this.pbCoin1.Name = "pbCoin1";
            this.pbCoin1.Size = new System.Drawing.Size(98, 132);
            this.pbCoin1.TabIndex = 1;
            this.pbCoin1.TabStop = false;
            // 
            // pbCoin2
            // 
            this.pbCoin2.BackColor = System.Drawing.Color.Transparent;
            this.pbCoin2.Image = global::VRAServer.Properties.Resources._0;
            this.pbCoin2.Location = new System.Drawing.Point(560, 0);
            this.pbCoin2.Name = "pbCoin2";
            this.pbCoin2.Size = new System.Drawing.Size(98, 132);
            this.pbCoin2.TabIndex = 2;
            this.pbCoin2.TabStop = false;
            // 
            // pbCoin3
            // 
            this.pbCoin3.BackColor = System.Drawing.Color.Transparent;
            this.pbCoin3.Image = global::VRAServer.Properties.Resources._0;
            this.pbCoin3.Location = new System.Drawing.Point(464, 0);
            this.pbCoin3.Name = "pbCoin3";
            this.pbCoin3.Size = new System.Drawing.Size(98, 132);
            this.pbCoin3.TabIndex = 3;
            this.pbCoin3.TabStop = false;
            // 
            // pbCoin4
            // 
            this.pbCoin4.BackColor = System.Drawing.Color.Transparent;
            this.pbCoin4.Image = global::VRAServer.Properties.Resources._0;
            this.pbCoin4.Location = new System.Drawing.Point(366, 0);
            this.pbCoin4.Name = "pbCoin4";
            this.pbCoin4.Size = new System.Drawing.Size(98, 132);
            this.pbCoin4.TabIndex = 4;
            this.pbCoin4.TabStop = false;
            // 
            // pbCoin5
            // 
            this.pbCoin5.BackColor = System.Drawing.Color.Transparent;
            this.pbCoin5.Image = global::VRAServer.Properties.Resources._0;
            this.pbCoin5.Location = new System.Drawing.Point(268, 0);
            this.pbCoin5.Name = "pbCoin5";
            this.pbCoin5.Size = new System.Drawing.Size(98, 132);
            this.pbCoin5.TabIndex = 5;
            this.pbCoin5.TabStop = false;
            // 
            // pbPlayer2
            // 
            this.pbPlayer2.Image = ((System.Drawing.Image)(resources.GetObject("pbPlayer2.Image")));
            this.pbPlayer2.Location = new System.Drawing.Point(520, 156);
            this.pbPlayer2.Name = "pbPlayer2";
            this.pbPlayer2.Size = new System.Drawing.Size(281, 113);
            this.pbPlayer2.TabIndex = 7;
            this.pbPlayer2.TabStop = false;
            this.pbPlayer2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbPlayer2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // btCostStart
            // 
            this.btCostStart.Location = new System.Drawing.Point(3, 3);
            this.btCostStart.Name = "btCostStart";
            this.btCostStart.Size = new System.Drawing.Size(75, 23);
            this.btCostStart.TabIndex = 0;
            this.btCostStart.Text = "유료시작";
            this.btCostStart.UseVisualStyleBackColor = true;
            this.btCostStart.Click += new System.EventHandler(this.btCostStart_Click);
            // 
            // pbPlayer1
            // 
            this.pbPlayer1.Image = ((System.Drawing.Image)(resources.GetObject("pbPlayer1.Image")));
            this.pbPlayer1.Location = new System.Drawing.Point(245, 156);
            this.pbPlayer1.Name = "pbPlayer1";
            this.pbPlayer1.Size = new System.Drawing.Size(262, 113);
            this.pbPlayer1.TabIndex = 6;
            this.pbPlayer1.TabStop = false;
            this.pbPlayer1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbPlayer1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // pnMovieSelect
            // 
            this.pnMovieSelect.BackColor = System.Drawing.Color.Transparent;
            this.pnMovieSelect.Controls.Add(this.pbMovie3);
            this.pnMovieSelect.Controls.Add(this.pbMovie2);
            this.pnMovieSelect.Controls.Add(this.pbMovie1);
            this.pnMovieSelect.Controls.Add(this.btSelectMovie);
            this.pnMovieSelect.Location = new System.Drawing.Point(0, 315);
            this.pnMovieSelect.Name = "pnMovieSelect";
            this.pnMovieSelect.Size = new System.Drawing.Size(1024, 300);
            this.pnMovieSelect.TabIndex = 4;
            this.pnMovieSelect.Visible = false;
            // 
            // pbMovie3
            // 
            this.pbMovie3.Location = new System.Drawing.Point(680, 0);
            this.pbMovie3.Name = "pbMovie3";
            this.pbMovie3.Size = new System.Drawing.Size(310, 234);
            this.pbMovie3.TabIndex = 3;
            this.pbMovie3.TabStop = false;
            this.pbMovie3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbMovie3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // pbMovie2
            // 
            this.pbMovie2.Location = new System.Drawing.Point(360, 0);
            this.pbMovie2.Name = "pbMovie2";
            this.pbMovie2.Size = new System.Drawing.Size(310, 234);
            this.pbMovie2.TabIndex = 2;
            this.pbMovie2.TabStop = false;
            this.pbMovie2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbMovie2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // pbMovie1
            // 
            this.pbMovie1.Location = new System.Drawing.Point(42, 0);
            this.pbMovie1.Name = "pbMovie1";
            this.pbMovie1.Size = new System.Drawing.Size(310, 234);
            this.pbMovie1.TabIndex = 1;
            this.pbMovie1.TabStop = false;
            this.pbMovie1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbMovie1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // btSelectMovie
            // 
            this.btSelectMovie.Location = new System.Drawing.Point(0, 3);
            this.btSelectMovie.Name = "btSelectMovie";
            this.btSelectMovie.Size = new System.Drawing.Size(36, 66);
            this.btSelectMovie.TabIndex = 0;
            this.btSelectMovie.Text = "영상선택";
            this.btSelectMovie.UseVisualStyleBackColor = true;
            this.btSelectMovie.Click += new System.EventHandler(this.btPreInfo_Click);
            // 
            // pnMovieGuide
            // 
            this.pnMovieGuide.BackColor = System.Drawing.Color.Transparent;
            this.pnMovieGuide.Controls.Add(this.btPreInfo);
            this.pnMovieGuide.Location = new System.Drawing.Point(453, 600);
            this.pnMovieGuide.Name = "pnMovieGuide";
            this.pnMovieGuide.Size = new System.Drawing.Size(160, 167);
            this.pnMovieGuide.TabIndex = 5;
            this.pnMovieGuide.Visible = false;
            // 
            // btPreInfo
            // 
            this.btPreInfo.Location = new System.Drawing.Point(3, 3);
            this.btPreInfo.Name = "btPreInfo";
            this.btPreInfo.Size = new System.Drawing.Size(75, 23);
            this.btPreInfo.TabIndex = 0;
            this.btPreInfo.Text = "사전고지";
            this.btPreInfo.UseVisualStyleBackColor = true;
            // 
            // pnMoviePlay
            // 
            this.pnMoviePlay.BackColor = System.Drawing.Color.Transparent;
            this.pnMoviePlay.Controls.Add(this.pbForceFinish);
            this.pnMoviePlay.Controls.Add(this.btMoviePlay);
            this.pnMoviePlay.Location = new System.Drawing.Point(800, 600);
            this.pnMoviePlay.Name = "pnMoviePlay";
            this.pnMoviePlay.Size = new System.Drawing.Size(224, 168);
            this.pnMoviePlay.TabIndex = 6;
            this.pnMoviePlay.Visible = false;
            // 
            // pbForceFinish
            // 
            this.pbForceFinish.Image = global::VRAServer.Properties.Resources.forcefinish;
            this.pbForceFinish.Location = new System.Drawing.Point(61, 96);
            this.pbForceFinish.Name = "pbForceFinish";
            this.pbForceFinish.Size = new System.Drawing.Size(162, 71);
            this.pbForceFinish.TabIndex = 1;
            this.pbForceFinish.TabStop = false;
            this.pbForceFinish.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ButtonDown);
            this.pbForceFinish.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ButtonUp);
            // 
            // btMoviePlay
            // 
            this.btMoviePlay.Location = new System.Drawing.Point(3, 3);
            this.btMoviePlay.Name = "btMoviePlay";
            this.btMoviePlay.Size = new System.Drawing.Size(75, 23);
            this.btMoviePlay.TabIndex = 0;
            this.btMoviePlay.Text = "상영중";
            this.btMoviePlay.UseVisualStyleBackColor = true;
            this.btMoviePlay.Click += new System.EventHandler(this.btFinish_Click);
            // 
            // pnFinish
            // 
            this.pnFinish.BackColor = System.Drawing.Color.Transparent;
            this.pnFinish.Controls.Add(this.btFinish);
            this.pnFinish.Location = new System.Drawing.Point(619, 600);
            this.pnFinish.Name = "pnFinish";
            this.pnFinish.Size = new System.Drawing.Size(176, 167);
            this.pnFinish.TabIndex = 7;
            this.pnFinish.Visible = false;
            // 
            // btFinish
            // 
            this.btFinish.Location = new System.Drawing.Point(3, 3);
            this.btFinish.Name = "btFinish";
            this.btFinish.Size = new System.Drawing.Size(75, 23);
            this.btFinish.TabIndex = 0;
            this.btFinish.Text = "종료";
            this.btFinish.UseVisualStyleBackColor = true;
            this.btFinish.Click += new System.EventHandler(this.Restart_Click);
            // 
            // VRAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.pnFinish);
            this.Controls.Add(this.pnMoviePlay);
            this.Controls.Add(this.pnMovieGuide);
            this.Controls.Add(this.pnMovieSelect);
            this.Controls.Add(this.pnBeginCost);
            this.Controls.Add(this.pnBeginFree);
            this.Controls.Add(this.pnBack);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "VRAForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "VR Attraction";
            this.TopMost = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VRAForm_FormClosed);
            this.pnBeginFree.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbFreePlayer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFreePlayer1)).EndInit();
            this.pnBeginCost.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCoin5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlayer1)).EndInit();
            this.pnMovieSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovie1)).EndInit();
            this.pnMovieGuide.ResumeLayout(false);
            this.pnMoviePlay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbForceFinish)).EndInit();
            this.pnFinish.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnBack;
        private System.Windows.Forms.Panel pnBeginFree;
        private System.Windows.Forms.Panel pnBeginCost;
        private System.Windows.Forms.Panel pnMovieSelect;
        private System.Windows.Forms.Panel pnMovieGuide;
        private System.Windows.Forms.Panel pnMoviePlay;
        private System.Windows.Forms.Panel pnFinish;
        private System.Windows.Forms.Button btFreeStart;
        private System.Windows.Forms.Button btCostStart;
        private System.Windows.Forms.Button btSelectMovie;
        private System.Windows.Forms.Button btPreInfo;
        private System.Windows.Forms.Button btMoviePlay;
        private System.Windows.Forms.Button btFinish;
        private System.Windows.Forms.PictureBox pbCoin5;
        private System.Windows.Forms.PictureBox pbCoin4;
        private System.Windows.Forms.PictureBox pbCoin3;
        private System.Windows.Forms.PictureBox pbCoin2;
        private System.Windows.Forms.PictureBox pbCoin1;
        private TouchButton pbPlayer2;
        private TouchButton pbPlayer1;
        private TouchButton pbMovie3;
        private TouchButton pbMovie2;
        private TouchButton pbMovie1;
        private TouchButton pbForceFinish;
        private TouchButton pbFreePlayer2;
        private TouchButton pbFreePlayer1;
    }
}