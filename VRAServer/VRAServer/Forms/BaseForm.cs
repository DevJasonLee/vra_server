﻿using System;
using System.Windows.Forms;
using VRAServer.Common;
using System.Net;
using System.Threading;
using System.IO;

namespace VRAServer
{

    public partial class BaseForm : Form
    {
        private const String PROJECT = "VRA"; // VRA, 방어진
        private const Int32 STX = 0x12344321;
        private const Int32 TEMP_LENGTH = 0x0000000C;
        private const Int32 RST_FAIL = 0x00000000;
        private const Int32 RST_OK = 0x00000001;

        private Log log = new Log("BaseForm");
        private Config config;
        private Server server;
        private VRAForm vraForm;
        private System.Windows.Forms.Timer AliveTimer;
        private int ServerState;    // 1: 환경설정, 2:체험하기

        private delegate void SetTextCallback(string text);

        public BaseForm()
        {
            InitializeComponent();
            PortFinder();
            InitConfig();
            LoadAccount();
        }

        private void BaseForm_Load(object sender, EventArgs e)
        {

        }

        private void PortFinder()
        {
            System.Diagnostics.Process ps = new System.Diagnostics.Process();
            ps.StartInfo.FileName = "PortFinder.exe";
            ps.StartInfo.WorkingDirectory = "./";
            ps.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            ps.Start();
            ps.WaitForExit();
        }

        private int PortLoad()
        {
            int count = 0;
            string line;
            using (StreamReader rdr = new StreamReader("port.cfg"))
            {
                cbMotion1P.Items.Clear();
                cbMotion2P.Items.Clear();
                cbMotion3P.Items.Clear();
                cbWind1P.Items.Clear();
                cbWind2P.Items.Clear();
                cbWind3P.Items.Clear();

                cbMotion1P.Items.Add("사용안함");
                cbMotion2P.Items.Add("사용안함");
                cbMotion3P.Items.Add("사용안함");
                cbWind1P.Items.Add("사용안함");
                cbWind2P.Items.Add("사용안함");
                cbWind3P.Items.Add("사용안함");

                int i = 0;
                while ((line = rdr.ReadLine()) != null)
                {
                    if( Boolean.Parse(line) )
                    {
                        String str = "COM" + i;

                        cbMotion1P.Items.Add(str);
                        cbMotion2P.Items.Add(str);
                        cbMotion3P.Items.Add(str);
                        cbWind1P.Items.Add(str);
                        cbWind2P.Items.Add(str);
                        cbWind3P.Items.Add(str);
                        count++;
                    }
                    i++;
                }
            }
            return count;
        }

        private void InitConfig()
        {
            vraForm = null;
            AliveTimer = new System.Windows.Forms.Timer();
            AliveTimer.Interval = 3000;
            AliveTimer.Tick += new EventHandler(Send_0016);

            ServerState = 0x01;
            // 초기값
            config = new Config {
                service     = "2인용 유료",
                productKey  = "",
                motion1P    = "사용안함",
                motion2P    = "사용안함",
                motion3P    = "사용안함",
                wind1P      = "사용안함",
                wind2P      = "사용안함",
                wind3P      = "사용안함",
                IP          = "192.168.0.10",
                port        = "8400",
                cost1P      = "3000",
                cost2P      = "5000",
                coin        = "0",
                coinUnit    = "1000",
                accountFolder = "./account",
                autoServer  = "FALSE",
                version     = "1.0.0"
            };

            PortLoad();

            try {
                config = SerializeXML.ReadFromXmlFile<Config>("config.xml");
            } catch(Exception ex) {
                MessageBox.Show("환경설정 파일을 불러올 수 없습니다.", "알림");
                log.Write(ex.ToString());
            } finally {

                switch (PROJECT)
                {
                    case "VRA":
                        lbMotion3P.Hide();
                        cbMotion3P.Hide();
                        btMotionTest3.Hide();
                        lbWind3P.Hide();
                        cbWind3P.Hide();
                        btWindTest3P.Hide();
                        cbService.Items.Add("1인용 무료");
                        cbService.Items.Add("2인용 무료");
                        cbService.Items.Add("1인용 유료");
                        cbService.Items.Add("2인용 유료");
                        cbService.Text = config.service;
                        if (cbService.Text.Equals("3인용 개별")) cbService.Text = "2인용 유료";
                        break;
                    case "방어진":
                        cbService.Items.Add("3인용 개별");
                        cbService.Text = "3인용 개별";
                        cbService.Enabled = false;
                        gbAccount.Hide();
                        tbProductKey.Enabled = false;
                        break;
                }
                
                tbProductKey.Text   = config.productKey;

                cbMotion1P.Text = config.motion1P;
                cbMotion2P.Text = config.motion2P;
                cbMotion3P.Text = config.motion3P;

                cbWind1P.Text = config.wind1P;
                cbWind2P.Text = config.wind2P;
                cbWind3P.Text = config.wind3P;

                tbIP.Text = config.IP;
                tbPort.Text = config.port;

                tbCoin.Text         = config.coin;
                tbCoinUnit.Text     = config.coinUnit;
                tbCost1P.Text       = config.cost1P;
                tbCost2P.Text       = config.cost2P;
                tbAccountFolder.Text = config.accountFolder;
                cbAutoServer.Checked = config.autoServer.Equals("TRUE") ? true : false;

                lbVersion.Text = "Version " + config.version;
            }

            if (cbAutoServer.Checked)
            {
                ServerStart();
            }
        }

        private void LoadAccount()
        {
            // TOTAL, TODAY 불러오기
            string[] account = Util.LoadAccount();
            Util.total = Int32.Parse(account[0]);
            Util.date = account[1];
            Util.today = Int32.Parse(account[2]);

            string now = DateTime.Now.ToShortDateString();
            if (!Util.date.Equals(now))
            {
                Util.date = now;
                Util.today = 0;
            }

            lbTotalCost.Text = Util.total.ToString();
            lbTodayCost.Text = Util.today.ToString();
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            ServerState = 0x02;
            if (!validate()) return;
            configuration();

            vraForm = new VRAForm(config);
            vraForm.formView += new VRAForm.FormView(vraFormView);
            vraForm.sendTo_0020 += new VRAForm.SendTo_0020(Send_0020);
            vraForm.sendTo_0022 += new VRAForm.SendTo_0022(Send_0022);
            vraForm.sendTo_0028 += new VRAForm.SendTo_0028(Send_0028);
            vraForm.Show();
            this.Visible = false;

            if (server == null) return;
            Send_0018();
        }

      
        private void vraFormView(bool flag)
        {
            if (flag)
            {
                LoadAccount();
                ServerState = 0x01;
                this.Visible = true;
                vraForm = null;

                if (server == null) return;
                Send_0018();
            }
        }

        private void configuration()
        {
            config.service = cbService.Text;
            config.productKey = tbProductKey.Text;

            config.motion1P = cbMotion1P.Text;
            config.motion2P = cbMotion2P.Text;
            config.motion3P = cbMotion3P.Text;

            config.wind1P = cbWind1P.Text;
            config.wind2P = cbWind2P.Text;
            config.wind3P = cbWind3P.Text;

            config.IP = tbIP.Text;
            config.port = tbPort.Text;

            config.coin = tbCoin.Text;
            config.coinUnit = tbCoinUnit.Text;
            config.cost1P = tbCost1P.Text;
            config.cost2P = tbCost2P.Text;            
            config.accountFolder = tbAccountFolder.Text;

            config.autoServer = cbAutoServer.Checked ? "TRUE" : "FALSE";
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            if (!validate()) return;
            configuration();

            SerializeXML.WriteToXmlFile<Config>("config.xml", config);
            MessageBox.Show("환경설정이 저장되었습니다.", "알림");
        }

        private void btFinish_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                tbAccountFolder.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void btServerStart_Click(object sender, EventArgs e)
        {
            ServerStart();            
        }

        private void ServerStart()
        {
            Console.WriteLine("ServerStart");
            SetText("* 서버가 시작되었습니다.");
            Thread.Sleep(100);

            //InitNetwork();

            server = new Server(10, 4096);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, Int32.Parse(config.port));
            server.Init();
            server.Start(ipep);
            server.eventDelegateProc += new EventDelegateProc(NetworkProc);

            // ALIVE 임시로 막아둠
            //AliveTimer.Start();

            btServerStart.Enabled = false;
            btServerStop.Enabled = true;
            btNetworkTest.Enabled = true;
        }
             
        private void btServerStop_Click(object sender, EventArgs e)
        {            
            ServerStop();
        }

        private void ServerStop()
        {
            Console.WriteLine("ServerStop");
            SetText("* 서버가 종료되었습니다.");

            Thread.Sleep(100);
            AliveTimer.Stop();

            server.CloseAllClientSocket();

            btServerStart.Enabled = true;
            btServerStop.Enabled = false;
            btNetworkTest.Enabled = false;
        }

        // 유효성 검사
        private Boolean validate()
        {
            // MessageBox 알림
            return true;
        }        

        public void SetText(string text)
        {
            if (rbClientList.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                if (rbClientList.TextLength > 0)
                {
                    rbClientList.AppendText("\n");
                }
                rbClientList.AppendText(text);
                rbClientList.ScrollToCaret();
            }
        }

        private void cbKeyDownDisable(object sender, KeyEventArgs e)
        {
            e.SuppressKeyPress = true;
        }

        // 네트워크 작업 처리
        public byte[] NetworkProc(byte[] packet)
        {
            BinaryReader reader = new BinaryReader(new MemoryStream(packet));
            Int32 stx = reader.ReadInt32();
            Int32 len = reader.ReadInt32();
            Int32 cmd = reader.ReadInt32();
            Console.Write(String.Format("[RECV] stx:0x{0:X} len:{1} cmd:0x{2:X} ", stx, len, cmd));
            int rst;

            switch (cmd)
            {
                case 0x00000010:
                    return Command_0010(reader);
                case 0x00000012:
                    return Command_0012(reader);
                case 0x00000014:
                    return Command_0014(reader);
                case 0x00000017:
                    Console.WriteLine(String.Format("rst:0x{0:X}", reader.ReadInt32()));
                    return null;
                case 0x00000019:                    
                    Console.WriteLine(String.Format("rst:0x{0:X}", reader.ReadInt32()));
                    return null;
                case 0x00000021:
                    Console.WriteLine(String.Format("rst:0x{0:X}", reader.ReadInt32()));
                    return null;
                case 0x00000023:
                    rst = reader.ReadInt32();
                    double ContentFPS = reader.ReadDouble();
                    Console.WriteLine(String.Format("rst:0x{0:X} ContentFPS:{1}", rst, ContentFPS));
                    return null;
                case 0x00000024:
                    return Command_0024(reader);
                case 0x00000026:
                    return Command_0026(reader);
                case 0x00000029:
                    Console.WriteLine(String.Format("rst:0x{0:X}", reader.ReadInt32()));
                    return null;
                default:
                    Console.WriteLine("(오류:알수없는 CMD값 입니다.)");
                    return null;
            }

        }

        // 서버 접속 알림
        private byte[] Command_0010(BinaryReader reader)
        {
            int hmdNo = reader.ReadInt32();
            Console.WriteLine(String.Format("hmdNo:0x{0:X}", hmdNo));

            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);

            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000011);
            writer.Write(RST_OK);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);

            return memory.ToArray();
        }

        // 정보 전송 요청
        private byte[] Command_0012(BinaryReader reader)
        {

            int hmdNo = reader.ReadInt32();
            byte[] licenseKey = reader.ReadBytes(16);
            Console.WriteLine(String.Format("hmdNo:0x{0:X} licenseKey:{1}", hmdNo, BitConverter.ToString(licenseKey)));

            /* 송신패킷 시작*/
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000013);
            writer.Write(RST_OK);

            int ServiceType;

            switch (config.service)
            {
                case "1인용 무료":
                    ServiceType = 0x01;
                    break;
                case "2인용 무료":
                    ServiceType = 0x02;
                    break;
                case "1인용 유료":
                    ServiceType = 0x11;
                    break;
                case "2인용 유료":
                    ServiceType = 0x12;
                    break;
                case "3인용 개별":// 울산 방어진 프로젝트
                    ServiceType = 0x23;
                    break;
                default:
                    ServiceType = 0x00;
                    break;
            }

            writer.Write(ServiceType);
            writer.Write(ServerState);
            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            /* 송신패킷 끝*/

            return memory.ToArray();
        }

        // 시간 동기화 요청
        private byte[] Command_0014(BinaryReader reader)
        {
            int TimeStamp = reader.ReadInt32();
            Console.WriteLine(String.Format("TimeStamp:0x{0:X}", TimeStamp));

            /* 송신패킷 시작*/
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000015);
            writer.Write(RST_OK);

            writer.Write(TimeStamp);
            int ServerTickCount = Environment.TickCount & Int32.MaxValue;
            writer.Write(ServerTickCount);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            /* 송신패킷 끝*/

            return memory.ToArray();
        }

        // ALIVE 요청
        void Send_0016(object sender, EventArgs e)
        {
            if (server == null)
            {
                AliveTimer.Stop();
                return;
            }

            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000016);
            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);

            server.SendAll(memory);
        }

        // 서버 변경 알림
        public void Send_0018()
        {            
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000018);
            writer.Write(ServerState);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            server.SendAll(memory);
        }

        // 지폐투입 알림
        private void Send_0020(int coinValue)
        {
            if (server == null) return;

            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000020);
            writer.Write(coinValue);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            server.SendAll(memory);
        }

        // 컨텐츠 시작 알림
        private void Send_0022(int ContentNo, int StartTime, int FastStart)
        {
            if (server == null) return;

            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);

            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000022);
            writer.Write(ContentNo);
            writer.Write(StartTime);
            writer.Write(FastStart);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);

            // HMD 개별로 알려주기
            server.SendAll(memory);
        }

        // 빠른시작요청
        private byte[] Command_0024(BinaryReader reader)
        {
            int hmdNo = reader.ReadInt32();
            int ContentNo = reader.ReadInt32();
            int StartTime = reader.ReadInt32();
            int rst = RST_OK;
            double ContentFPS = reader.ReadDouble();
            Console.WriteLine(String.Format("hmdNo:0x{0:X} ContentNo:{1} StartTime:{2} ContentFPS:{3}", hmdNo, ContentNo, StartTime, ContentFPS));

            if (vraForm == null) rst = 0x10;
            else vraForm.MoviePlay(hmdNo, ContentNo, StartTime, ContentFPS);

            /* 송신패킷 시작*/
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000025);
            writer.Write(rst);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            /* 송신패킷 끝*/

            return memory.ToArray();
        }

        // 컨텐츠 제어
        private byte[] Command_0026(BinaryReader reader)
        {
            int hmdNo = reader.ReadInt32();
            int ContentNo = reader.ReadInt32();
            int ControlCMD = reader.ReadInt32();
            Console.WriteLine(String.Format("hmdNo:0x{0:X} ContentNo:{1} ControlCMD:{2}", hmdNo, ContentNo, ControlCMD));

            if(vraForm != null)
            {
                vraForm.Control(hmdNo, ControlCMD);
            }

            /* 송신패킷 시작*/
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000027);
            writer.Write(RST_OK);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            /* 송신패킷 끝*/

            return memory.ToArray();
        }

        // 컨텐츠 강제종료
        private void Send_0028()
        {
            if (server == null) return;
            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(STX);
            writer.Write(TEMP_LENGTH);
            writer.Write(0x00000028);

            writer.Seek(4, SeekOrigin.Begin);
            writer.Write((Int32)writer.BaseStream.Length);
            server.SendAll(memory);
        }

        private void btPortFinder_Click(object sender, EventArgs e)
        {
            btPortFinder.Enabled = false;
            PortFinder();
            int count = PortLoad();
            btPortFinder.Enabled = true;
            if( count == 0) MessageBox.Show("연결된 4D 기기가 없습니다.", "완료");
            else MessageBox.Show(count + "개의 포트를 찾았습니다.", "완료");
        }

        private void btMotionTest1P_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "X ", cbMotion1P.Text);
        }

        private void btMotionTest2P_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "X ", cbMotion2P.Text);
        }

        private void btMotionTest3_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "X ", cbMotion3P.Text);
        }

        private void btWindTest1P_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "W ", cbWind1P.Text);
        }

        private void btWindTest2P_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "W ", cbWind2P.Text);
        }

        private void btWindTest3P_Click(object sender, EventArgs e)
        {
            Test((Button)sender, "W ", cbWind3P.Text);
        }

        private void Test(Button bt, String arg, String text)
        {
            if (text.Equals("사용안함"))
            {
                MessageBox.Show("포트를 지정해 주세요.", "경고");
                return;
            }

            int port = Int32.Parse(text.Replace("COM", ""));

            bt.Enabled = false;
            System.Diagnostics.Process ps = new System.Diagnostics.Process();
            ps.StartInfo.FileName = "PortTest.exe";
            ps.StartInfo.WorkingDirectory = "./";
            ps.StartInfo.Arguments = arg + port;
            ps.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            ps.Start();
            ps.WaitForExit();
            MessageBox.Show("테스트를 완료했습니다.", "완료");
            bt.Enabled = true;
        }
    }
}
