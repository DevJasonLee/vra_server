﻿namespace VRAServer
{
    partial class BaseForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cbAutoServer = new System.Windows.Forms.CheckBox();
            this.btServerStop = new System.Windows.Forms.Button();
            this.btServerStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbClientList = new System.Windows.Forms.RichTextBox();
            this.tbIP = new IPAddressControlLib.IPAddressControl();
            this.btNetworkTest = new System.Windows.Forms.Button();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbProductKey = new System.Windows.Forms.TextBox();
            this.btSave = new System.Windows.Forms.Button();
            this.btFinish = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btMotionTest3 = new System.Windows.Forms.Button();
            this.cbMotion3P = new System.Windows.Forms.ComboBox();
            this.lbMotion3P = new System.Windows.Forms.Label();
            this.btMotionTest2P = new System.Windows.Forms.Button();
            this.cbMotion2P = new System.Windows.Forms.ComboBox();
            this.cbMotion1P = new System.Windows.Forms.ComboBox();
            this.btMotionTest1P = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.gbAccount = new System.Windows.Forms.GroupBox();
            this.lbToday = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tbCoinUnit = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tbAccountFolder = new System.Windows.Forms.TextBox();
            this.btFolder = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.tbCoin = new System.Windows.Forms.TextBox();
            this.tbCost2P = new System.Windows.Forms.TextBox();
            this.tbCost1P = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btWindTest1P = new System.Windows.Forms.Button();
            this.cbWind1P = new System.Windows.Forms.ComboBox();
            this.cbWind2P = new System.Windows.Forms.ComboBox();
            this.btStart = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btPortFinder = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbService = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btWindTest3P = new System.Windows.Forms.Button();
            this.btWindTest2P = new System.Windows.Forms.Button();
            this.cbWind3P = new System.Windows.Forms.ComboBox();
            this.lbWind3P = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lbVersion = new System.Windows.Forms.Label();
            this.lbTotalCost = new System.Windows.Forms.Label();
            this.lbTodayCost = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.gbAccount.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbAutoServer);
            this.groupBox2.Controls.Add(this.btServerStop);
            this.groupBox2.Controls.Add(this.btServerStart);
            this.groupBox2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox2.Location = new System.Drawing.Point(10, 336);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(290, 86);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "서버";
            // 
            // cbAutoServer
            // 
            this.cbAutoServer.AutoSize = true;
            this.cbAutoServer.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbAutoServer.Location = new System.Drawing.Point(180, 54);
            this.cbAutoServer.Name = "cbAutoServer";
            this.cbAutoServer.Size = new System.Drawing.Size(100, 17);
            this.cbAutoServer.TabIndex = 104;
            this.cbAutoServer.Text = "자동 서버 시작";
            this.cbAutoServer.UseVisualStyleBackColor = true;
            // 
            // btServerStop
            // 
            this.btServerStop.Enabled = false;
            this.btServerStop.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btServerStop.Location = new System.Drawing.Point(150, 21);
            this.btServerStop.Name = "btServerStop";
            this.btServerStop.Size = new System.Drawing.Size(130, 25);
            this.btServerStop.TabIndex = 103;
            this.btServerStop.Text = "서버 정지";
            this.btServerStop.UseVisualStyleBackColor = true;
            this.btServerStop.Click += new System.EventHandler(this.btServerStop_Click);
            // 
            // btServerStart
            // 
            this.btServerStart.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btServerStart.Location = new System.Drawing.Point(8, 21);
            this.btServerStart.Name = "btServerStart";
            this.btServerStart.Size = new System.Drawing.Size(130, 25);
            this.btServerStart.TabIndex = 102;
            this.btServerStart.Text = "서버 시작";
            this.btServerStart.UseVisualStyleBackColor = true;
            this.btServerStart.Click += new System.EventHandler(this.btServerStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(10, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "제품 키";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbClientList);
            this.groupBox3.Controls.Add(this.tbIP);
            this.groupBox3.Controls.Add(this.btNetworkTest);
            this.groupBox3.Controls.Add(this.tbPort);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox3.Location = new System.Drawing.Point(310, 10);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(290, 160);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "네트워크";
            // 
            // rbClientList
            // 
            this.rbClientList.Location = new System.Drawing.Point(6, 85);
            this.rbClientList.Name = "rbClientList";
            this.rbClientList.Size = new System.Drawing.Size(277, 69);
            this.rbClientList.TabIndex = 109;
            this.rbClientList.Text = "";
            // 
            // tbIP
            // 
            this.tbIP.AllowInternalTab = false;
            this.tbIP.AutoHeight = true;
            this.tbIP.BackColor = System.Drawing.SystemColors.Window;
            this.tbIP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tbIP.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.tbIP.Enabled = false;
            this.tbIP.Location = new System.Drawing.Point(60, 15);
            this.tbIP.MinimumSize = new System.Drawing.Size(84, 22);
            this.tbIP.Name = "tbIP";
            this.tbIP.ReadOnly = false;
            this.tbIP.Size = new System.Drawing.Size(118, 22);
            this.tbIP.TabIndex = 108;
            this.tbIP.Text = "...";
            // 
            // btNetworkTest
            // 
            this.btNetworkTest.Enabled = false;
            this.btNetworkTest.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btNetworkTest.Location = new System.Drawing.Point(184, 14);
            this.btNetworkTest.Name = "btNetworkTest";
            this.btNetworkTest.Size = new System.Drawing.Size(99, 52);
            this.btNetworkTest.TabIndex = 105;
            this.btNetworkTest.Text = "테스트";
            this.btNetworkTest.UseVisualStyleBackColor = true;
            // 
            // tbPort
            // 
            this.tbPort.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbPort.Location = new System.Drawing.Point(60, 44);
            this.tbPort.MaxLength = 5;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(53, 22);
            this.tbPort.TabIndex = 7;
            this.tbPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(10, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "PORT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(10, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "IP";
            // 
            // tbProductKey
            // 
            this.tbProductKey.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbProductKey.Location = new System.Drawing.Point(60, 45);
            this.tbProductKey.Margin = new System.Windows.Forms.Padding(0);
            this.tbProductKey.MaxLength = 20;
            this.tbProductKey.Name = "tbProductKey";
            this.tbProductKey.Size = new System.Drawing.Size(170, 22);
            this.tbProductKey.TabIndex = 1;
            // 
            // btSave
            // 
            this.btSave.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btSave.Location = new System.Drawing.Point(429, 399);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(79, 23);
            this.btSave.TabIndex = 201;
            this.btSave.Text = "설정 저장";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btFinish
            // 
            this.btFinish.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btFinish.Location = new System.Drawing.Point(514, 399);
            this.btFinish.Name = "btFinish";
            this.btFinish.Size = new System.Drawing.Size(86, 23);
            this.btFinish.TabIndex = 202;
            this.btFinish.Text = "종료";
            this.btFinish.UseVisualStyleBackColor = true;
            this.btFinish.Click += new System.EventHandler(this.btFinish_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btMotionTest3);
            this.groupBox4.Controls.Add(this.cbMotion3P);
            this.groupBox4.Controls.Add(this.lbMotion3P);
            this.groupBox4.Controls.Add(this.btMotionTest2P);
            this.groupBox4.Controls.Add(this.cbMotion2P);
            this.groupBox4.Controls.Add(this.cbMotion1P);
            this.groupBox4.Controls.Add(this.btMotionTest1P);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox4.Location = new System.Drawing.Point(10, 91);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(290, 117);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Sym4D-X100(모션)";
            // 
            // btMotionTest3
            // 
            this.btMotionTest3.Location = new System.Drawing.Point(180, 86);
            this.btMotionTest3.Name = "btMotionTest3";
            this.btMotionTest3.Size = new System.Drawing.Size(99, 21);
            this.btMotionTest3.TabIndex = 104;
            this.btMotionTest3.Text = "테스트";
            this.btMotionTest3.UseVisualStyleBackColor = true;
            this.btMotionTest3.Click += new System.EventHandler(this.btMotionTest3_Click);
            // 
            // cbMotion3P
            // 
            this.cbMotion3P.FormattingEnabled = true;
            this.cbMotion3P.Location = new System.Drawing.Point(60, 86);
            this.cbMotion3P.Name = "cbMotion3P";
            this.cbMotion3P.Size = new System.Drawing.Size(113, 21);
            this.cbMotion3P.TabIndex = 103;
            this.cbMotion3P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // lbMotion3P
            // 
            this.lbMotion3P.AutoSize = true;
            this.lbMotion3P.Location = new System.Drawing.Point(11, 89);
            this.lbMotion3P.Name = "lbMotion3P";
            this.lbMotion3P.Size = new System.Drawing.Size(45, 13);
            this.lbMotion3P.TabIndex = 102;
            this.lbMotion3P.Text = "3P 모션";
            // 
            // btMotionTest2P
            // 
            this.btMotionTest2P.Location = new System.Drawing.Point(179, 57);
            this.btMotionTest2P.Name = "btMotionTest2P";
            this.btMotionTest2P.Size = new System.Drawing.Size(100, 21);
            this.btMotionTest2P.TabIndex = 101;
            this.btMotionTest2P.Text = "테스트";
            this.btMotionTest2P.UseVisualStyleBackColor = true;
            this.btMotionTest2P.Click += new System.EventHandler(this.btMotionTest2P_Click);
            // 
            // cbMotion2P
            // 
            this.cbMotion2P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbMotion2P.FormattingEnabled = true;
            this.cbMotion2P.Location = new System.Drawing.Point(60, 57);
            this.cbMotion2P.Name = "cbMotion2P";
            this.cbMotion2P.Size = new System.Drawing.Size(113, 21);
            this.cbMotion2P.TabIndex = 3;
            this.cbMotion2P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // cbMotion1P
            // 
            this.cbMotion1P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbMotion1P.FormattingEnabled = true;
            this.cbMotion1P.Location = new System.Drawing.Point(60, 27);
            this.cbMotion1P.Name = "cbMotion1P";
            this.cbMotion1P.Size = new System.Drawing.Size(113, 21);
            this.cbMotion1P.TabIndex = 2;
            this.cbMotion1P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // btMotionTest1P
            // 
            this.btMotionTest1P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btMotionTest1P.Location = new System.Drawing.Point(180, 27);
            this.btMotionTest1P.Name = "btMotionTest1P";
            this.btMotionTest1P.Size = new System.Drawing.Size(99, 21);
            this.btMotionTest1P.TabIndex = 100;
            this.btMotionTest1P.Text = "테스트";
            this.btMotionTest1P.UseVisualStyleBackColor = true;
            this.btMotionTest1P.Click += new System.EventHandler(this.btMotionTest1P_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(10, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "2P 모션";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(10, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "1P 모션";
            // 
            // gbAccount
            // 
            this.gbAccount.Controls.Add(this.lbTodayCost);
            this.gbAccount.Controls.Add(this.lbTotalCost);
            this.gbAccount.Controls.Add(this.lbToday);
            this.gbAccount.Controls.Add(this.lbTotal);
            this.gbAccount.Controls.Add(this.label17);
            this.gbAccount.Controls.Add(this.label16);
            this.gbAccount.Controls.Add(this.label15);
            this.gbAccount.Controls.Add(this.label14);
            this.gbAccount.Controls.Add(this.tbCoinUnit);
            this.gbAccount.Controls.Add(this.label13);
            this.gbAccount.Controls.Add(this.tbAccountFolder);
            this.gbAccount.Controls.Add(this.btFolder);
            this.gbAccount.Controls.Add(this.label12);
            this.gbAccount.Controls.Add(this.tbCoin);
            this.gbAccount.Controls.Add(this.tbCost2P);
            this.gbAccount.Controls.Add(this.tbCost1P);
            this.gbAccount.Controls.Add(this.label7);
            this.gbAccount.Controls.Add(this.label6);
            this.gbAccount.Controls.Add(this.label5);
            this.gbAccount.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbAccount.Location = new System.Drawing.Point(310, 176);
            this.gbAccount.Name = "gbAccount";
            this.gbAccount.Size = new System.Drawing.Size(290, 217);
            this.gbAccount.TabIndex = 0;
            this.gbAccount.TabStop = false;
            this.gbAccount.Text = "결제";
            // 
            // lbToday
            // 
            this.lbToday.AutoSize = true;
            this.lbToday.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.lbToday.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbToday.Location = new System.Drawing.Point(14, 39);
            this.lbToday.Name = "lbToday";
            this.lbToday.Size = new System.Drawing.Size(69, 12);
            this.lbToday.TabIndex = 114;
            this.lbToday.Text = "금일 이용금액";
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.lbTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbTotal.Location = new System.Drawing.Point(14, 19);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(69, 12);
            this.lbTotal.TabIndex = 113;
            this.lbTotal.Text = "전체 이용금액";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.Gray;
            this.label17.Location = new System.Drawing.Point(146, 151);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(129, 13);
            this.label17.TabIndex = 112;
            this.label17.Text = "시작전 미리 투입된 금액";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.Gray;
            this.label16.Location = new System.Drawing.Point(146, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(140, 13);
            this.label16.TabIndex = 111;
            this.label16.Text = "동전이나 지폐의 투입 단위";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.Gray;
            this.label15.Location = new System.Drawing.Point(146, 93);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(124, 13);
            this.label15.TabIndex = 110;
            this.label15.Text = "2인용 체험시 지불 금액";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.Gray;
            this.label14.Location = new System.Drawing.Point(146, 64);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(124, 13);
            this.label14.TabIndex = 109;
            this.label14.Text = "1인용 체험시 지불 금액";
            // 
            // tbCoinUnit
            // 
            this.tbCoinUnit.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbCoinUnit.Location = new System.Drawing.Point(93, 117);
            this.tbCoinUnit.MaxLength = 5;
            this.tbCoinUnit.Name = "tbCoinUnit";
            this.tbCoinUnit.Size = new System.Drawing.Size(49, 22);
            this.tbCoinUnit.TabIndex = 10;
            this.tbCoinUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(12, 151);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(55, 13);
            this.label13.TabIndex = 9;
            this.label13.Text = "시작 금액";
            // 
            // tbAccountFolder
            // 
            this.tbAccountFolder.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbAccountFolder.Location = new System.Drawing.Point(12, 187);
            this.tbAccountFolder.Name = "tbAccountFolder";
            this.tbAccountFolder.Size = new System.Drawing.Size(190, 22);
            this.tbAccountFolder.TabIndex = 107;
            // 
            // btFolder
            // 
            this.btFolder.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btFolder.Location = new System.Drawing.Point(208, 186);
            this.btFolder.Name = "btFolder";
            this.btFolder.Size = new System.Drawing.Size(75, 23);
            this.btFolder.TabIndex = 108;
            this.btFolder.Text = "찾아보기";
            this.btFolder.UseVisualStyleBackColor = true;
            this.btFolder.Click += new System.EventHandler(this.btFolder_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(11, 171);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(77, 13);
            this.label12.TabIndex = 6;
            this.label12.Text = "정산폴더 위치";
            // 
            // tbCoin
            // 
            this.tbCoin.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbCoin.Location = new System.Drawing.Point(93, 146);
            this.tbCoin.MaxLength = 5;
            this.tbCoin.Name = "tbCoin";
            this.tbCoin.Size = new System.Drawing.Size(49, 22);
            this.tbCoin.TabIndex = 11;
            this.tbCoin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCost2P
            // 
            this.tbCost2P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbCost2P.Location = new System.Drawing.Point(93, 88);
            this.tbCost2P.MaxLength = 5;
            this.tbCost2P.Name = "tbCost2P";
            this.tbCost2P.Size = new System.Drawing.Size(49, 22);
            this.tbCost2P.TabIndex = 9;
            this.tbCost2P.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCost1P
            // 
            this.tbCost1P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbCost1P.Location = new System.Drawing.Point(93, 59);
            this.tbCost1P.MaxLength = 5;
            this.tbCost1P.Name = "tbCost1P";
            this.tbCost1P.Size = new System.Drawing.Size(49, 22);
            this.tbCost1P.TabIndex = 8;
            this.tbCost1P.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(12, 122);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "투입 단위";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(12, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "2인 금액";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(12, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "1인 금액";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(10, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "1P 바람";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(10, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "2P 바람";
            // 
            // btWindTest1P
            // 
            this.btWindTest1P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btWindTest1P.Location = new System.Drawing.Point(180, 27);
            this.btWindTest1P.Name = "btWindTest1P";
            this.btWindTest1P.Size = new System.Drawing.Size(99, 21);
            this.btWindTest1P.TabIndex = 101;
            this.btWindTest1P.Text = "테스트";
            this.btWindTest1P.UseVisualStyleBackColor = true;
            this.btWindTest1P.Click += new System.EventHandler(this.btWindTest1P_Click);
            // 
            // cbWind1P
            // 
            this.cbWind1P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbWind1P.FormattingEnabled = true;
            this.cbWind1P.Location = new System.Drawing.Point(60, 27);
            this.cbWind1P.Name = "cbWind1P";
            this.cbWind1P.Size = new System.Drawing.Size(113, 21);
            this.cbWind1P.TabIndex = 4;
            this.cbWind1P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // cbWind2P
            // 
            this.cbWind2P.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbWind2P.FormattingEnabled = true;
            this.cbWind2P.Location = new System.Drawing.Point(60, 57);
            this.cbWind2P.Name = "cbWind2P";
            this.cbWind2P.Size = new System.Drawing.Size(113, 21);
            this.cbWind2P.TabIndex = 5;
            this.cbWind2P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // btStart
            // 
            this.btStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btStart.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btStart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btStart.Location = new System.Drawing.Point(10, 430);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(590, 66);
            this.btStart.TabIndex = 200;
            this.btStart.Text = "시작하기";
            this.btStart.UseVisualStyleBackColor = false;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btPortFinder);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbProductKey);
            this.groupBox1.Controls.Add(this.cbService);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(290, 76);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "일반";
            // 
            // btPortFinder
            // 
            this.btPortFinder.Location = new System.Drawing.Point(233, 15);
            this.btPortFinder.Name = "btPortFinder";
            this.btPortFinder.Size = new System.Drawing.Size(51, 52);
            this.btPortFinder.TabIndex = 2;
            this.btPortFinder.Text = "포트 검색";
            this.btPortFinder.UseVisualStyleBackColor = true;
            this.btPortFinder.Click += new System.EventHandler(this.btPortFinder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(10, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "서비스";
            // 
            // cbService
            // 
            this.cbService.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbService.FormattingEnabled = true;
            this.cbService.Location = new System.Drawing.Point(60, 15);
            this.cbService.Name = "cbService";
            this.cbService.Size = new System.Drawing.Size(95, 21);
            this.cbService.TabIndex = 0;
            this.cbService.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btWindTest3P);
            this.groupBox6.Controls.Add(this.btWindTest2P);
            this.groupBox6.Controls.Add(this.cbWind3P);
            this.groupBox6.Controls.Add(this.lbWind3P);
            this.groupBox6.Controls.Add(this.cbWind2P);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.btWindTest1P);
            this.groupBox6.Controls.Add(this.cbWind1P);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox6.Location = new System.Drawing.Point(10, 213);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(290, 118);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Sym4D-W100(바람)";
            // 
            // btWindTest3P
            // 
            this.btWindTest3P.Location = new System.Drawing.Point(180, 84);
            this.btWindTest3P.Name = "btWindTest3P";
            this.btWindTest3P.Size = new System.Drawing.Size(99, 23);
            this.btWindTest3P.TabIndex = 105;
            this.btWindTest3P.Text = "테스트";
            this.btWindTest3P.UseVisualStyleBackColor = true;
            this.btWindTest3P.Click += new System.EventHandler(this.btWindTest3P_Click);
            // 
            // btWindTest2P
            // 
            this.btWindTest2P.Location = new System.Drawing.Point(180, 55);
            this.btWindTest2P.Name = "btWindTest2P";
            this.btWindTest2P.Size = new System.Drawing.Size(99, 23);
            this.btWindTest2P.TabIndex = 104;
            this.btWindTest2P.Text = "테스트";
            this.btWindTest2P.UseVisualStyleBackColor = true;
            this.btWindTest2P.Click += new System.EventHandler(this.btWindTest2P_Click);
            // 
            // cbWind3P
            // 
            this.cbWind3P.FormattingEnabled = true;
            this.cbWind3P.Location = new System.Drawing.Point(60, 86);
            this.cbWind3P.Name = "cbWind3P";
            this.cbWind3P.Size = new System.Drawing.Size(113, 21);
            this.cbWind3P.TabIndex = 103;
            this.cbWind3P.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbKeyDownDisable);
            // 
            // lbWind3P
            // 
            this.lbWind3P.AutoSize = true;
            this.lbWind3P.Location = new System.Drawing.Point(11, 89);
            this.lbWind3P.Name = "lbWind3P";
            this.lbWind3P.Size = new System.Drawing.Size(45, 13);
            this.lbWind3P.TabIndex = 102;
            this.lbWind3P.Text = "3P 바람";
            // 
            // lbVersion
            // 
            this.lbVersion.AutoSize = true;
            this.lbVersion.Font = new System.Drawing.Font("맑은 고딕", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lbVersion.ForeColor = System.Drawing.Color.Gray;
            this.lbVersion.Location = new System.Drawing.Point(527, 503);
            this.lbVersion.Name = "lbVersion";
            this.lbVersion.Size = new System.Drawing.Size(0, 13);
            this.lbVersion.TabIndex = 203;
            // 
            // lbTotalCost
            // 
            this.lbTotalCost.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.lbTotalCost.Location = new System.Drawing.Point(90, 18);
            this.lbTotalCost.Name = "lbTotalCost";
            this.lbTotalCost.Size = new System.Drawing.Size(80, 13);
            this.lbTotalCost.TabIndex = 115;
            this.lbTotalCost.Text = "0";
            this.lbTotalCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbTodayCost
            // 
            this.lbTodayCost.Font = new System.Drawing.Font("맑은 고딕", 7F);
            this.lbTodayCost.Location = new System.Drawing.Point(90, 38);
            this.lbTodayCost.Name = "lbTodayCost";
            this.lbTodayCost.Size = new System.Drawing.Size(80, 13);
            this.lbTodayCost.TabIndex = 116;
            this.lbTodayCost.Text = "0";
            this.lbTodayCost.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 523);
            this.Controls.Add(this.lbVersion);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.gbAccount);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btStart);
            this.Controls.Add(this.btFinish);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "BaseForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VRA SERVER";
            this.Load += new System.EventHandler(this.BaseForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.gbAccount.ResumeLayout(false);
            this.gbAccount.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbProductKey;
        private System.Windows.Forms.Button btServerStart;
        private System.Windows.Forms.Button btServerStop;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.Button btFinish;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox gbAccount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbAutoServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbWind2P;
        private System.Windows.Forms.ComboBox cbMotion2P;
        private System.Windows.Forms.ComboBox cbWind1P;
        private System.Windows.Forms.ComboBox cbMotion1P;
        private System.Windows.Forms.Button btWindTest1P;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btMotionTest1P;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCoin;
        private System.Windows.Forms.TextBox tbCost2P;
        private System.Windows.Forms.TextBox tbCost1P;
        private System.Windows.Forms.Button btNetworkTest;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.TextBox tbAccountFolder;
        private System.Windows.Forms.Button btFolder;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbService;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbCoinUnit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private IPAddressControlLib.IPAddressControl tbIP;
        private System.Windows.Forms.Label lbVersion;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.RichTextBox rbClientList;
        private System.Windows.Forms.Label lbToday;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.Button btMotionTest3;
        private System.Windows.Forms.ComboBox cbMotion3P;
        private System.Windows.Forms.Label lbMotion3P;
        private System.Windows.Forms.Button btMotionTest2P;
        private System.Windows.Forms.Button btWindTest3P;
        private System.Windows.Forms.Button btWindTest2P;
        private System.Windows.Forms.ComboBox cbWind3P;
        private System.Windows.Forms.Label lbWind3P;
        private System.Windows.Forms.Button btPortFinder;
        private System.Windows.Forms.Label lbTodayCost;
        private System.Windows.Forms.Label lbTotalCost;
    }
}

