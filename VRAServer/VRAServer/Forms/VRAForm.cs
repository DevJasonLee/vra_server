﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using VRAServer.Common;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using System.Threading;
using VRAServer.Sym4D;

namespace VRAServer
{
    /*
        pnBeginFree     : 시작 무료
        pnBeginCost     : 시작 유료(지폐투입)
        pnMovieSelect   : 영상 선택
        pnMovieGuide    : 사전고지
        pnMoviePlay     : 영상 재생
        pnFinish        : 영상 종료
    */

    public partial class VRAForm : Form
    {
        private Log log = new Log("VRAForm");
        private Config config;

        private const Int32 STX = 0x12344321;
        private const Int32 TEMP_LENGTH = 0x0000000C;
        private const Int32 RST_FAIL = 0x00000000;
        private const Int32 RST_OK = 0x00000001;

        public delegate void FormView(bool flag);
        public delegate void SendTo_0020(int coinValue);
        public delegate void SendTo_0022(int ContentNo, int StartTime,int FastSTart);
        public delegate void SendTo_0028();

        public event FormView formView;
        public event SendTo_0020 sendTo_0020;
        public event SendTo_0022 sendTo_0022;
        public event SendTo_0028 sendTo_0028;

        private int iCost1P, iCost2P, iCoinUnit, iCoin;
        private string service;
        private Bitmap[] bNumbers, bSelNum, bSelNumDn;
        private List<Image[]> movieList;
        
        private Boolean bMoviePlayer1P = false;
        private Boolean bMoviePlayer2P = false;
        private int iMovieSelected = -1;

        private MoviePlay[] moviePlay;
        private Thread[] thread;
        private int MoviePlayTime;

        private Object thisLock = new Object();
        System.Windows.Forms.Timer FinishTimer = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer RestartTimer = new System.Windows.Forms.Timer();
        

        public VRAForm(Config config)
        {
            InitializeComponent();
            Init(config);
            Restart();            
            //InitSym4D();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        protected override void OnNotifyMessage(Message m)
        {
            if (m.Msg != 0x14) base.OnNotifyMessage(m);
        }

        private void Init(Config config)
        {
            this.SetStyle(System.Windows.Forms.ControlStyles.UserPaint, true);
            this.SetStyle(System.Windows.Forms.ControlStyles.OptimizedDoubleBuffer | System.Windows.Forms.ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(System.Windows.Forms.ControlStyles.EnableNotifyMessage, true);


            moviePlay = new MoviePlay[3];
            thread = new Thread[2];

            this.config = config;
            this.KeyPreview = true;
            this.KeyPress += new KeyPressEventHandler(VRAForm_KeyPress);

            RestartTimer.Tick += new EventHandler(Restart_Tick);
            FinishTimer.Tick += new EventHandler(Finish_Tick);

            this.iCost1P    = Int32.Parse(config.cost1P);
            this.iCost2P    = Int32.Parse(config.cost2P);
            this.iCoinUnit  = Int32.Parse(config.coinUnit);
            this.iCoin      = Int32.Parse(config.coin);
            this.service= config.service;

            bNumbers = new Bitmap[10] {
                Properties.Resources._0,
                Properties.Resources._1,
                Properties.Resources._2,
                Properties.Resources._3,
                Properties.Resources._4,
                Properties.Resources._5,
                Properties.Resources._6,
                Properties.Resources._7,
                Properties.Resources._8,
                Properties.Resources._9
            };

            bSelNum = new Bitmap[10] {
                Properties.Resources.sel_0,
                Properties.Resources.sel_1,
                Properties.Resources.sel_2,
                Properties.Resources.sel_3,
                Properties.Resources.sel_4,
                Properties.Resources.sel_5,
                Properties.Resources.sel_6,
                Properties.Resources.sel_7,
                Properties.Resources.sel_8,
                Properties.Resources.sel_9
            };

            bSelNumDn = new Bitmap[10] {
                Properties.Resources.sel_0_dn,
                Properties.Resources.sel_1_dn,
                Properties.Resources.sel_2_dn,
                Properties.Resources.sel_3_dn,
                Properties.Resources.sel_4_dn,
                Properties.Resources.sel_5_dn,
                Properties.Resources.sel_6_dn,
                Properties.Resources.sel_7_dn,
                Properties.Resources.sel_8_dn,
                Properties.Resources.sel_9_dn
            };

            int cost1P, cost2P;

            try
            {
                cost1P = (int)((float)iCost1P * 0.001f) % 10;
                cost2P = (int)((float)iCost2P * 0.001f) % 10;
            } catch(Exception ex) {
                cost1P = 3;
                cost2P = 5;
            }
            
            Bitmap player1 = (Bitmap)Properties.Resources.player1.Clone();
            Bitmap player1_dn = (Bitmap)Properties.Resources.player1_dn.Clone();
            Bitmap player2 = (Bitmap)Properties.Resources.player2.Clone();
            Bitmap player2_dn = (Bitmap)Properties.Resources.player2_dn.Clone();

            Graphics g = Graphics.FromImage(player1);
            g.DrawImage(bSelNum[cost1P], new Rectangle(106, 56, 32, 40));
            g = Graphics.FromImage(player1_dn);
            g.DrawImage(bSelNumDn[cost1P], new Rectangle(106, 56, 32, 40));
            g = Graphics.FromImage(player2);
            g.DrawImage(bSelNum[cost2P], new Rectangle(130, 56, 32, 40));
            g = Graphics.FromImage(player2_dn);
            g.DrawImage(bSelNumDn[cost2P], new Rectangle(130, 56, 32, 40));
            g.Dispose();

            SetPictureBox(pbPlayer1, player1);
            SetPictureBox(pbPlayer2, player2);

            pbFreePlayer1.Tag = new Image[] { Properties.Resources.freeplayer1, Properties.Resources.freeplayer1_dn };
            pbFreePlayer2.Tag = new Image[] { Properties.Resources.freeplayer2, Properties.Resources.freeplayer2_dn };
            pbPlayer1.Tag = new Image[] { player1, player1_dn };
            pbPlayer2.Tag = new Image[] { player2, player2_dn };
            pbForceFinish.Tag = new Image[] { Properties.Resources.forcefinish, Properties.Resources.forcefinish_dn };

            movieList = new List<Image[]>();

            SetPictureBoxVisibility(pbMovie1, false);
            SetPictureBoxVisibility(pbMovie2, false);
            SetPictureBoxVisibility(pbMovie3, false);

            for ( int i = 0; i < 20; ++i )
            {
                String movie = "./Movies/" + i + ".png";
                String movie_dn = "./Movies/" + i + "_dn.png";
                if( File.Exists(movie) && File.Exists(movie_dn) )
                {
                    movieList.Add(new Image[2] { new Bitmap(movie), new Bitmap(movie_dn) } );
                }

            }

            if (movieList.Count >= 1)
            {
                SetPictureBoxVisibility(pbMovie1, true);
                pbMovie1.Tag = movieList[0];
                SetPictureBox(pbMovie1, movieList[0][0]);
            }

            if (movieList.Count >= 2)
            {
                SetPictureBoxVisibility(pbMovie2, true);
                pbMovie2.Tag = movieList[1];
                SetPictureBox(pbMovie2, movieList[1][0]);
            }

            if (movieList.Count >= 3)
            {
                SetPictureBoxVisibility(pbMovie3, true);
                pbMovie3.Tag = movieList[2];
                SetPictureBox(pbMovie3, movieList[2][0]);
            }

            if (movieList.Count >= 4)
            {
                // 좌, 우 이동 가능...
            }
            
        }

        void Restart()
        {

            TouchButton[] buttons = new TouchButton[]
            {
                this.pbPlayer2, this.pbPlayer1,this.pbMovie3, this.pbMovie2, this.pbMovie1, this.pbForceFinish, this.pbFreePlayer2, this.pbFreePlayer1
            };

            foreach(TouchButton pb in buttons)
            {                
                Image[] images = (Image[])pb.Tag;
                SetPictureBox(pb, images[0]);
            }

            bMoviePlayer1P = false;
            bMoviePlayer2P = false;
            iMovieSelected = -1;

            switch (service)
            {
                case "1인용 무료":
                    SetBackground(Properties.Resources.beginfree_bg);                    
                    SetPanel(pnBeginFree, true);
                    SetPictureBoxVisibility(pbFreePlayer2, false);
                    break;
                case "2인용 무료":
                    SetBackground(Properties.Resources.beginfree_bg);
                    SetPanel(pnBeginFree, true);
                    break;
                case "1인용 유료":
                    SetBackground(Properties.Resources.begincost_bg);
                    SetPanel(pnBeginCost, true);
                    break;
                case "2인용 유료":
                    SetBackground(Properties.Resources.begincost_bg);
                    SetPanel(pnBeginCost, true);
                    break;
                case "3인용 개별":
                    SetBackground(Properties.Resources.movieguide_bg);
                    SetPanel(pnMovieGuide, true);
                    break;
                default:
                    break;
            }

            SetPanel(pnFinish, false);

            UpdateCoin();
        }


        private void Magic_Click(object sender, EventArgs e)
        {            
            this.Close();
        }

        // 지폐투입
        void VRAForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case '5':

                    string now = DateTime.Now.ToShortDateString();
                    if (!Util.date.Equals(now))
                    {
                        Util.date = now;
                        Util.today = 0;
                    }
                    Util.total += iCoinUnit;
                    Util.today += iCoinUnit;
                    Util.SaveAccount(Util.total, Util.today);

                    iCoin += iCoinUnit;                    
                    e.Handled = true;
                    UpdateCoin();
                    break;
            }
        }

        private void btFreeStart_Click(object sender, EventArgs e)
        {
            FreeStart();
        }

        public void FreeStart()
        {
            SetPanel(pnBeginFree, false);
            SetBackground(Properties.Resources.movieselect_bg);
            SetPanel(pnMovieSelect, true);
        }

        private void btCostStart_Click(object sender, EventArgs e)
        {
            CostStart();
        }

        public void CostStart()
        {
            SetPanel(pnBeginCost, false);
            SetBackground(Properties.Resources.begincost_bg);
            SetPanel(pnMovieSelect, true);
        }

        private void btSelectMovie_Click(object sender, EventArgs e)
        {
            SelectMovie();
        }

        public void SelectMovie()
        {
            SetPanel(pnMovieSelect, false);
            SetBackground(Properties.Resources.movieselect_bg);
            SetPanel(pnMovieSelect, true);
        }

        private void btPreInfo_Click(object sender, EventArgs e)
        {
            PreInfo();
        }

        public void PreInfo()
        {
            SetPanel(pnMovieSelect, false);
            SetBackground(Properties.Resources.movieguide_bg);
            SetPanel(pnMovieGuide, true);
        }

        //private void btMoviePlay_Click(object sender, EventArgs e)
        //{
        //    int ServerTickCount = Environment.TickCount & Int32.MaxValue;
        //    MoviePlay(1, 1, ServerTickCount + 5000, 29.97d);
        //    MoviePlay(2, 1, ServerTickCount + 5000, 29.97d);
        //    //MoviePlay(3, 3, ServerTickCount, 29.97d);
        //}

        private void btFinish_Click(object sender, EventArgs e)
        {
            Finish();
        }

        public void Finish()
        {
            SetPanel(pnMoviePlay, false);
            SetBackground(Properties.Resources.finish_bg);
            SetPanel(pnFinish, true);
        }

        private void Restart_Click(object sender, EventArgs e)
        {            
            Restart();
        }

        private void ButtonDown(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            //Image[] images = (Image[])pb.Tag;
            //pb.Image = images[1];
        }

        private void ButtonUp(object sender, MouseEventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            //Image[] images = (Image[])pb.Tag;
            //pb.Image = images[0];

            switch (pb.Name)
            {
                case "pbFreePlayer1":
                    bMoviePlayer1P = true;
                    bMoviePlayer2P = false;
                    BeginFree();
                    break;
                case "pbFreePlayer2":
                    bMoviePlayer1P = true;
                    bMoviePlayer2P = true;
                    BeginFree();
                    break;
                case "pbPlayer1":
                    bMoviePlayer1P = true;
                    bMoviePlayer2P = false;
                    BeginCost(iCost1P);
                    break;
                case "pbPlayer2":
                    bMoviePlayer1P = true;
                    bMoviePlayer2P = true;
                    BeginCost(iCost2P);
                    break;
                case "pbMovie1":
                    iMovieSelected = 1;// (int)pbMovie1.Tag;
                    MovieSelect();
                    break;
                case "pbMovie2":
                    iMovieSelected = 2;// (int)pbMovie2.Tag;
                    MovieSelect();
                    break;
                case "pbMovie3":
                    iMovieSelected = 3;// (int)pbMovie3.Tag;
                    MovieSelect();
                    break;
                case "pbForceFinish":
                    ForceFinish();
                    break;                    
                default:
                    break;
            }
        }

        protected override void WndProc(ref System.Windows.Forms.Message msg)
        {
            const int WM_POINTERDOWN = 0x0246;
            const int WM_POINTERUP = 0x247;
            switch (msg.Msg)
            {
            case WM_POINTERDOWN:
                {
                    IntPtr xy = msg.LParam;
                    int x = unchecked((short)(long)xy);
                    int y = unchecked((short)((long)xy >> 16));

                    int xPos = x;
                    int yPos = y;

                    //if (IS_POINTER_PRIMARYBUTTON_WPARAM(wParam))
                    //{
                    //    // process pointer down, similar to mouse left button down
                    //}
                    //else if (IS_POINTER_SECONDARYBUTTON_WPARAM(wParam))
                    //{
                    //    // process pointer down, similar to mouse right button down
                    //}

                    //mouse_event(MOUSEEVENTF_LEFTDOWN, x, y, 0, 0);

                    PictureBox pb = pbPlayer1;
                    Image[] images = (Image[])pb.Tag;
                    SetPictureBox(pb, images[1]);
                    break;
                }
                case WM_POINTERUP:
                {
                    IntPtr xy = msg.LParam;
                    int x = unchecked((short)(long)xy);
                    int y = unchecked((short)((long)xy >> 16));

                    int xPos = x;
                    int yPos = y;
                    //mouse_event(MOUSEEVENTF_LEFTUP, x, y, 0, 0);

                    PictureBox pb = pbPlayer2;
                    Image[] images = (Image[])pb.Tag;
                    SetPictureBox(pb, images[0]);
                    break;
                }
            }
            base.WndProc(ref msg);
        }

        private void BeginFree()
        {
            SetPanel(pnBeginFree, false);
            SetBackground(Properties.Resources.movieselect_bg);
            SetPanel(pnMovieSelect, true);
        }

        private void BeginCost(int cost)
        {
            iCoin -= cost;
            SetPanel(pnBeginCost, false);
            if (iCoin < 0) iCoin = 0;
            SetBackground(Properties.Resources.begincost_bg);
            SetPanel(pnMovieSelect, true);
        }
        
        // 컨텐츠 선택시
        private void MovieSelect()
        {
            SetPanel(pnMovieSelect, false);
            SetBackground(Properties.Resources.movieguide_bg);
            SetPanel(pnMovieGuide, true);

            MoviePlayTime = (Environment.TickCount & Int32.MaxValue) + MovieStartTerm;
            if (sendTo_0022 != null) this.sendTo_0022(0x01, MoviePlayTime, 0x00);

            if(bMoviePlayer1P) MoviePlay(1, iMovieSelected, MoviePlayTime, 29.97d);
            if(bMoviePlayer2P) MoviePlay(2, iMovieSelected, MoviePlayTime, 29.97d);

        }

        private int MovieStartTerm = 5000;
        private int MovieEndTerm = 3000;

        public void MoviePlay(int HMDNo, int ContentNo, int StartTime, double ContentFPS)
        {
            if (!service.Equals("3인용 개별"))
            {
                SetPanel(pnMovieGuide, false);
                SetBackground(Properties.Resources.moiveplay_bg);
                SetPanel(pnMoviePlay, true);
            }

            if (moviePlay[HMDNo - 1] != null) moviePlay[HMDNo - 1].ThreadStop();
            moviePlay[HMDNo - 1] = new MoviePlay(config, HMDNo, ContentNo, StartTime);

            thread[HMDNo - 1] = new Thread(moviePlay[HMDNo - 1].Play);
            thread[HMDNo - 1].Start();

            if (!service.Equals("3인용 개별"))
            {
                if(HMDNo == 1)
                {
                    // 타이머. 플레이 시간이 끝난뒤 ForceFinish 호출
                    // 종료 타임 : 시작대기 5~20초 + 플레이타임 + 종료대기 5~20초
                    FinishTimer.Interval = MovieStartTerm + (int)(moviePlay[0].GetPlayTime() * 1000f) + MovieEndTerm;
                    FinishTimer.Start();
                    
                }                
            }
        }

        private void Finish_Tick(object sender, EventArgs e)
        {
            ((System.Windows.Forms.Timer)sender).Stop();
            ForceFinish();
        }

        private void ForceFinish()
        {
            FinishTimer.Stop();

            if (moviePlay[0] != null ) moviePlay[0].ThreadStop();
            if(moviePlay[1] != null ) moviePlay[1].ThreadStop();

            SetPanel(pnMoviePlay, false);
            SetBackground(Properties.Resources.finish_bg);
            SetPanel(pnFinish, true);

            if (sendTo_0028 != null) this.sendTo_0028();
            RestartTimer.Interval = 5000;            
            RestartTimer.Start();
        }

        private void Restart_Tick(object sender, EventArgs e)
        {
            ((System.Windows.Forms.Timer)sender).Stop();
            Restart();
        }

        private void UpdateCoin()
        {
            int coin = Math.Min(99999, iCoin);

            float pos1 = (float)(coin % 10);
            float pos2 = (float)(coin % 100) * 0.1f;
            float pos3 = (float)(coin % 1000) * 0.01f;
            float pos4 = (float)(coin % 10000) * 0.001f;
            float pos5 = (float)(coin % 100000) * 0.0001f;

            SetPictureBox(pbCoin1, bNumbers[(int)pos1]);
            SetPictureBox(pbCoin2, bNumbers[(int)pos2]);
            SetPictureBox(pbCoin3, bNumbers[(int)pos3]);
            SetPictureBox(pbCoin4, bNumbers[(int)pos4]);
            SetPictureBox(pbCoin5, bNumbers[(int)pos5]);

            switch (service)
            {
                case "1인용 유료":
                    SetPictureBoxVisibility(pbPlayer2, false);
                    if (coin >= iCost1P) pbPlayer1.Show();
                    else SetPictureBoxVisibility(pbPlayer1, false);
                    break;
                case "2인용 유료":
                    if (coin >= iCost1P) pbPlayer1.Show();
                    else SetPictureBoxVisibility(pbPlayer1, false);
                    if (coin >= iCost2P) pbPlayer2.Show();
                    else SetPictureBoxVisibility(pbPlayer2, false);
                    break;
                default:
                    break;
            }

            if(sendTo_0020 != null ) this.sendTo_0020(iCoin);
        }

        public void Control(int HMDNo, int ControlCode)
        {
            moviePlay[HMDNo - 1].Control(ControlCode);
        }

        //private void SaveAccount()
        //{
        //    Excel.Application _eApp;
        //    Excel.Workbook _eWorkbook;
        //    Excel.Worksheet _eWorkSheet;
        //    string[,] _data;
        //    _eApp = new Excel.Application();
        //    _eWorkbook = _eApp.Workbooks.Add(true);
        //    _eWorkSheet = (Excel.Worksheet)_eWorkbook.Sheets[1]; // Excel Sheet 배열은 1부터 시작한다.
        //    _data = new string[3, 5];
        //    try
        //    {
        //        _data[0, 0] = "번호";
        //        _data[0, 1] = "이름";
        //        _data[0, 2] = "학년";
        //        _data[0, 3] = "성별";
        //        _data[0, 4] = "키";
        //        _data[1, 0] = "1";
        //        _data[1, 1] = "홍길동";
        //        _data[1, 2] = "1";
        //        _data[1, 3] = "남";
        //        _data[1, 4] = "170";
        //        _data[2, 0] = "2";
        //        _data[2, 1] = "놀부";
        //        _data[2, 2] = "3";
        //        _data[2, 3] = "남";
        //        _data[2, 4] = "175";
        //        _eWorkSheet.get_Range("A1:E3").Value2 = _data;
        //        // FileFormat에 XlFileFormat.xlWorkbookDefault을 입력하면 .xlsx ( 통합양식 )으로 처리되고 Excel.XlFileFormat.xlWorkbookNormal을 입력하면 .xls ( 2003 이하? )으로 처리된다.
        //        _eWorkbook.SaveAs("test", Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, false, Excel.XlSaveAsAccessMode.xlShared, false, false, Type.Missing, Type.Missing, Type.Missing);                
        //    }
        //    catch
        //    {
        //    }
        //    finally
        //    {
        //        // 종료할때는 Excel Application을 종료해준다.
        //        // 해당 처리를 안하면 보이지 않은 엑셀이 종료되지 않음
        //        _eWorkbook.Close(false, Type.Missing, Type.Missing);
        //        _eApp.Quit();
        //    }
        //}

        private void VRAForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.formView(true);
        }

        delegate void PanelArgReturningVoidDelegate(Panel panel, bool bIsShow);
        delegate void BGlArgReturningVoidDelegate(Bitmap bitmap);
        delegate void PictureBoxArgReturningVoidDelegate(PictureBox pb, Image image);
        delegate void PictureBoxVisibilityArgReturningVoidDelegate(PictureBox pb, bool bIsShow);

        private void SetPanel(Panel panel, bool bIsShow)
        {
            if (panel.InvokeRequired)
            {
                PanelArgReturningVoidDelegate d = new PanelArgReturningVoidDelegate(SetPanel);
                this.Invoke(d, new object[] { panel, bIsShow });
            }
            else
            {
                if(bIsShow) panel.Show();
                else panel.Hide();
            }
        }

        private void SetBackground(Bitmap bitmap)
        {
            if (this.InvokeRequired)
            {
                BGlArgReturningVoidDelegate d = new BGlArgReturningVoidDelegate(SetBackground);
                this.Invoke(d, new object[] { bitmap });
            }
            else
            {
                this.BackgroundImage = bitmap;
            }
        }

        private void SetPictureBox(PictureBox pb, Image image)
        {
            if (this.InvokeRequired)
            {
                PictureBoxArgReturningVoidDelegate d = new PictureBoxArgReturningVoidDelegate(SetPictureBox);
                this.Invoke(d, new object[] { pb, image });
            }
            else
            {
                pb.Image = image;
            }
        }

        private void SetPictureBoxVisibility(PictureBox pb, bool bIsShow)
        {
            if (this.InvokeRequired)
            {
                PictureBoxVisibilityArgReturningVoidDelegate d = new PictureBoxVisibilityArgReturningVoidDelegate(SetPictureBoxVisibility);
                this.Invoke(d, new object[] { pb, bIsShow });
            }
            else
            {
                if (bIsShow) pb.Show();
                else pb.Hide();
            }
        }


    }




}