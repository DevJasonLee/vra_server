﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VRAServer.Common
{
    class Util
    {
        static public int total = 0;
        static public int today = 0;
        static public string date = "";

        static public string[] LoadAccount()
        {
            string[] rst = new string[3];
            using (StreamReader rdr = new StreamReader("Account/sum.cfg"))
            {
                rst[0] = rdr.ReadLine();
                rst[1] = rdr.ReadLine();
                rst[2] = rdr.ReadLine();
            }            


            return rst;
        }

        static public void SaveAccount(int total, int today)
        {
            string date = DateTime.Now.ToShortDateString();
            using (StreamWriter wr = new StreamWriter("Account/sum.cfg"))
            {
                wr.WriteLine(total);
                wr.WriteLine(date);
                wr.WriteLine(today);
            }
        }
    }
}
