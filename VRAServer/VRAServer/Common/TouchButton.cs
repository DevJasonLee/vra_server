﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VRAServer.Common
{
    class TouchButton : System.Windows.Forms.PictureBox
    {
        protected override void WndProc(ref System.Windows.Forms.Message msg)
        {
            const int WM_POINTERDOWN = 0x0246;
            const int WM_POINTERUP = 0x247;
            switch (msg.Msg)
            {
                case WM_POINTERDOWN:
                    {
                        PictureBox pb = (PictureBox)this;
                        Image[] images = (Image[])pb.Tag;
                        pb.Image = images[1];
                        break;
                    }
                case WM_POINTERUP:
                    {
                        PictureBox pb = (PictureBox)this;
                        Image[] images = (Image[])pb.Tag;
                        pb.Image = images[0];
                        break;
                    }
            }
            base.WndProc(ref msg);
        }
    }
}
