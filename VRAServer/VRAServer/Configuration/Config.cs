﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VRAServer
{
    [Serializable]
    public class Config
    {

        public string service { get; set; }
        public string productKey { get; set; }
        public string motion1P { get; set; }
        public string motion2P { get; set; }
        public string motion3P { get; set; }
        public string wind1P { get; set; }
        public string wind2P { get; set; }
        public string wind3P { get; set; }
        public string IP { get; set; }
        public string port { get; set; }
        public string cost1P { get; set; }
        public string cost2P { get; set; }
        public string coin { get; set; }
        public string coinUnit { get; set; }
        public string accountFolder { get; set; }
        public string autoServer { get; set; }
        public string version { get; set; }
    }
}
